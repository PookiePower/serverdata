// TMW-2 Script
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Hurnscald doesn't have a well, but have a MAGIC FOUNTAIN ***
//    Req. Int 40 to finish Tier Elevation quest.

014-3,151,117,0	script	Fountain#Hurns	NPC_NO_SPRITE,{
    if (ST_TIER == 5 && gettimetick(2) < QUEST_ELEVARTEMPO) goto L_Tier2;

    mesn l("Mana Saulc");
    mesc l("Go away, I am too magical for you. %%n"); // quote
    close;

    // TODO FIXME: We don't have a "Magic" water, and we might add
    // Mahed or Mahad, another well master (Mehoud? Muhoud?)
    input .@count;

    if (.@count == 0)
        close;
    .@Cost = .@count * .COST_PER_BOTTLE;
    .@empty = countitem("EmptyBottle");

    if (.@empty < .@count)
        goto L_NotEnoughBottles;
    if (Zeny < .@Cost)
        goto L_NotEnoughMoney;
    getinventorylist;
    inventoryplace BottleOfTonoriWater, .@count;

    Zeny=Zeny-.@Cost;
    delitem "EmptyBottle", .@count;
    getitem "BottleOfTonoriWater", .@count;
    close;

L_NotEnoughBottles:
    mes "";
    mesn;
    mes l("You don't have that many empty bottles!");
    close;

L_NotEnoughMoney:
    mes "";
    mesn;
    mes l("You don't have enough gold! You need @@ gp.", .@Cost);
    close;


L_Tier2:
    if (readparam(bInt) < 10) goto L_Dumb;
    mesn;
    mesc l("WHAT ARE YOU GOING TO DO?");
    next;
    select
        l("Do nothing"), // 1
        l("Drink the potion"), // 2
        l("Pour the potion"), // 3
        l("Wash yourself with the potion"), // 4
        l("Burn the potion"), // 5
        l("Drink the potion, and say magic words"), // 6
        l("Say magic words"), // 7
        l("Do a weird dance"), // 8
        l("Blame Saulc"); // 9

    mes "";
    if (@menu == 1)
        close;

    if (@menu == 3)
        goto L_Tier2Ok;

    if (@menu <= 6)
        goto L_Failed;

    if (@menu == 9)
        mesc l("Whatever you're blaming Saulc about, this one time, he is innocent.");
    mesc l("Nothing happens.");
    close;

L_Fail2:
    mesc l("Your low intelligence prevents anything from happening with you.");
    mes "";

L_Failed:
    mesn strcharinfo(0);
    mesq l("Ah no... That's not what I had to do... I wasted the potion...");
    QUEST_ELEVARTEMPO=gettimetick(2);
    close;

L_Dumb:
    mesn strcharinfo(0);
    mesq l("Hello there pretty fountain, what about granting me magic?");
    next;
    mesc l("Unsurprisingly, nothing happens.");
    close;

L_Tier2Ok:
    mesc l("You pour the whole potion on the fountain.");
    next;
    if (readparam(bInt) < 20) goto L_Fail2;
    mesc l("You hear birds singing! That is what you had to do!");
    next;
    if (readparam(bInt) < 30) goto L_Fail2;
    mesc l("Your body starts to glow. You're not sure why, the fountain did that!");
    next;
    if (readparam(bInt) < 40) goto L_Fail2;
    mesn;
    mes l("I am the Magic Fountain of Hurnscald. You look qualified.");
    next;
    mesn;
    mes l("Your next step is to get the book of the Second Sage Of Fate.");
    next;
    mesn;
    mes l("I predict you're closer to find the Secret Of Mana, and therefore, be part on saving our world.");
    next;
    mesn;
    mes l("I will empower you with raw mana, and your time will start running out again.");
    next;
    // 30~60 s + 4~6 minutes to finish in average. Each level grants 4~6 extra seconds.
    // You will also get extra time for int, but randomly.
    QUEST_ELEVARTEMPO=gettimetick(2)+rand(30,60)+rand((BaseLevel*4),(BaseLevel*6))+rand(1,readparam(bInt));
    ST_TIER=6;
    // Ref. 240 xp. You will be penalized with 1 xp for over-level. (waw...)
    if (BaseLevel < 300)
        getexp 300-BaseLevel, 0;
    mesn;
    mesq l("Run like the wind, @@! For you shall have only @@ to finish the ritual!", strcharinfo(0), FuzzyTime(QUEST_ELEVARTEMPO,2,2));
    close;

OnInit:
    .COST_PER_BOTTLE = 0;
    .sex = G_OTHER;
    .distance = 3;

    end;
}
