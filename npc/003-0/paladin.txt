// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Leader of the PALADIN class

003-0,34,37,0	script	Paladin Master	NPC_PLAYER,{
    /*
    if (!is_staff())
        goto L_Close;
    */
    if (!(MAGIC_SUBCLASS & CL_PALADIN))
        goto L_SignUp;
    goto L_Member;

// Sign Up
L_SignUp:
    // Not allowed if subclass filled or not from main class
    if (total_subclass() >= max_subclass() || getskilllv(MAGIC_WARRIOR) < 2)
        goto L_Close;
    mesn;
    mesq l("Hey there! Do you want to join the Paladin Class?");
    mesc l("Warning: If you join a subclass, you can't leave it later!"), 1;
    next;
    if (askyesno() != ASK_YES)
        close;
    // TODO: Requeriment for signing up to a subclass? Or is the tier + skill quest hard enough?
    MAGIC_SUBCLASS=MAGIC_SUBCLASS|CL_PALADIN;
    mesn;
    mesq l("Welcome to the paladin guild!");
    close;

// Close
L_Close:
    goodbye;
    closedialog;
    close;

L_Missing:
    mesn;
    mesq l("Hey hey! You don't have that stuff, CAN'T YOU READ?!");
    percentheal 0, -10;
    next;
    goto L_Member;

// Membership area
// Paladin
//  CR_TRUST (raise Max HP in 200 and Holy Resistance in 5%, passive)
//  AL_ANGELUS (DEF Increase 5% for	15s/LVL, 14x14 area for PARTY)
//  PR_REDEMPTIO (suicide with death penalty. Revive dead party members on a 29x29 area. Min. 1% xp. 0.01% xp penalty reduction per revive)
//  MER_INCAGI (raise agi and move speed for 20s/LVL. Have an HP cost.)
//  SM_BASH up to level 4 (+220% dmg and +20% acc). PS. If you have MP, SM_BASH is very powerful.

L_Member:
    mesn;
    mesq l("Hey there! Do you want to learn new skills for a very small teaching fee?");
    select
        rif(sk_intcost(SM_BASH) && getskilllv(SM_BASH) < (3+degree_subclass()/2), l("Improve Bash Skill")),
        rif(sk_intcost(AL_ANGELUS) && sk_canlvup(AL_ANGELUS), l("Improve Party Area Defense")),
        "","",
        //rif(sk_intcost(MER_INCAGI) && sk_canlvup(MER_INCAGI), l("Improve Increase Agility")),
        //rif(sk_intcost(PR_REDEMPTIO) && !getskilllv(PR_REDEMPTIO), l("Learn Redemption")),
        rif(sk_intcost(CR_TRUST) && !getskilllv(CR_TRUST), l("Learn Last Standing Man")),
        l("Leave Subclass"),
        l("Nothing at the moment.");
    mes "";
    switch (@menu) {
        case 1:
            mesc l("[Bash]");
            mesc l("Blow with increased attack and precision.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(ManaPiouFeathers),  (getskilllv(SM_BASH)+1)*15,  getitemlink(ManaPiouFeathers));
            mesc l("@@/@@ @@", countitem(CaveSnakeSkin),     (getskilllv(SM_BASH)+1)*4,  getitemlink(CaveSnakeSkin));
            mesc l("@@/@@ @@", countitem(RubyPowder),        (getskilllv(SM_BASH)+1)*2,  getitemlink(RubyPowder));
            mesc l("@@/@@ @@", countitem(StrengthPotion),    (getskilllv(SM_BASH)+1)*2,  getitemlink(StrengthPotion));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(ManaPiouFeathers) < (getskilllv(SM_BASH)+1)*15 ||
                    countitem(CaveSnakeSkin)    < (getskilllv(SM_BASH)+1)*4 ||
                    countitem(RubyPowder)       < (getskilllv(SM_BASH)+1)*2 ||
                    countitem(StrengthPotion)   < (getskilllv(SM_BASH)+1)*2) goto L_Missing;

                delitem PiberriesInfusion, (getskilllv(SM_BASH)+1)*15;
                delitem CaveSnakeSkin,     (getskilllv(SM_BASH)+1)*4;
                delitem RubyPowder,        (getskilllv(SM_BASH)+1)*2;
                delitem StrengthPotion,    (getskilllv(SM_BASH)+1)*2;

                sk_lvup(SM_BASH);

                next;
            }
            break;
        case 2:
            mesc l("[Party Area Defense]");
            mesc l("Raises defense of the whole party in 5% for a while.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(PiberriesInfusion), (getskilllv(AL_ANGELUS)+1)*10, getitemlink(PiberriesInfusion));
            mesc l("@@/@@ @@", countitem(BlackMambaEgg),     (getskilllv(AL_ANGELUS)+1)*2,  getitemlink(BlackMambaEgg));
            mesc l("@@/@@ @@", countitem(MoubooSteak),       (getskilllv(AL_ANGELUS)+1)*4,  getitemlink(MoubooSteak));
            mesc l("@@/@@ @@", countitem(IronIngot),         (getskilllv(AL_ANGELUS)+1)*1,  getitemlink(IronIngot));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(PiberriesInfusion) < (getskilllv(AL_ANGELUS)+1)*10 ||
                    countitem(BlackMambaEgg)     < (getskilllv(AL_ANGELUS)+1)*2 ||
                    countitem(MoubooSteak)       < (getskilllv(AL_ANGELUS)+1)*4 ||
                    countitem(IronIngot)         < (getskilllv(AL_ANGELUS)+1)*1) goto L_Missing;

                delitem PiberriesInfusion, (getskilllv(AL_ANGELUS)+1)*10;
                delitem BlackMambaEgg,     (getskilllv(AL_ANGELUS)+1)*2;
                delitem MoubooSteak,       (getskilllv(AL_ANGELUS)+1)*4;
                delitem IronIngot,         (getskilllv(AL_ANGELUS)+1)*1;

                sk_lvup(AL_ANGELUS);

                next;
            }
            break;
        case 3:
            mesc l("[Increase Agility]");
            mesc l("Temporaly raise your agility and move speed, in exchange of HP.");
            mes "";
            // TODO: Requirem, quest, agree
            sk_lvup(MER_INCAGI);
            break;
        case 4:
            mesc l("[Redemption]");
            mesc l("Kills yourself, but revives everyone in a range. You'll take the death penalty.");
            mes "";
            // TODO: Requirem, quest, agree
            sk_lvup(PR_REDEMPTIO);
            break;
        case 5:
            mesc l("[Last Standing Man]");
            mesc l("Raises max HP and holy resistance. Passive.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(Cheese),            (getskilllv(CR_TRUST)+1)*50, getitemlink(Cheese));
            mesc l("@@/@@ @@", countitem(Coral),             (getskilllv(CR_TRUST)+1)*30, getitemlink(Coral));
            mesc l("@@/@@ @@", countitem(PiberriesInfusion), (getskilllv(CR_TRUST)+1)*20, getitemlink(PiberriesInfusion));
            mesc l("@@/@@ @@", countitem(FluoPowder),        (getskilllv(CR_TRUST)+1)*15, getitemlink(FluoPowder));
            mesc l("@@/@@ @@", countitem(HastePotion),       (getskilllv(CR_TRUST)+1)*10, getitemlink(HastePotion));
            mesc l("@@/@@ @@", countitem(DiamondPowder),     (getskilllv(CR_TRUST)+1)*3,  getitemlink(DiamondPowder));
            mesc l("@@/@@ @@", countitem(GrassCarp),         (getskilllv(CR_TRUST)+1)*1,  getitemlink(GrassCarp));
            mesc l("@@/@@ @@", countitem(ElixirOfLife),      (getskilllv(CR_TRUST)+1)*1,  getitemlink(ElixirOfLife));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(Cheese)               < (getskilllv(CR_TRUST)+1)*50 ||
                    countitem(Coral)                < (getskilllv(CR_TRUST)+1)*30 ||
                    countitem(PiberriesInfusion)    < (getskilllv(CR_TRUST)+1)*20 ||
                    countitem(FluoPowder)           < (getskilllv(CR_TRUST)+1)*15 ||
                    countitem(HastePotion)          < (getskilllv(CR_TRUST)+1)*10 ||
                    countitem(DiamondPowder)        < (getskilllv(CR_TRUST)+1)*3 ||
                    countitem(GrassCarp)            < (getskilllv(CR_TRUST)+1)*1 ||
                    countitem(ElixirOfLife)         < (getskilllv(CR_TRUST)+1)*1) goto L_Missing;

                delitem Cheese,             (getskilllv(CR_TRUST)+1)*50;
                delitem Coral,              (getskilllv(CR_TRUST)+1)*30;
                delitem PiberriesInfusion,  (getskilllv(CR_TRUST)+1)*20;
                delitem FluoPowder,         (getskilllv(CR_TRUST)+1)*15;
                delitem HastePotion,        (getskilllv(CR_TRUST)+1)*10;
                delitem DiamondPowder,      (getskilllv(CR_TRUST)+1)*3;
                delitem GrassCarp,          (getskilllv(CR_TRUST)+1)*1;
                delitem ElixirOfLife,       (getskilllv(CR_TRUST)+1)*1;

                sk_lvup(CR_TRUST);

                next;
            }
            break;
        case 6:
            // All skills related may include the basic class skills if they're related.
            mesc l("WARNING: If you leave the subclass, you'll lose all skills related to it!"), 1;
            mesc l("This cannot be undone. Are you sure?"), 1;
            mes "";
            if (askyesno() == ASK_YES) {
                mes "";
                if (validatepin()) {
                    skill CR_TRUST, 0, 0;
                    skill AL_ANGELUS, 0, 0;
                    skill PR_REDEMPTIO, 0, 0;
                    skill MER_INCAGI, 0, 0;
                    skill SM_BASH, 2, 0;
                    MAGIC_SUBCLASS=MAGIC_SUBCLASS^CL_PALADIN;
                    mesc l("You abandoned the PALADIN class!"), 1;
                    close;
                } else {
                    mesc l("Failed to validate pin. Aborting.");
                    next;
                }
            } else {
                mes "";
                mesc l("Operation aborted. Phew!");
                next;
            }
            break;
        default:
            goto L_Close;
    }

    goto L_Member;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, WarlordHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GoldenWarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansChaps);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;
}

