// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Leader of the PRIEST class

003-0,58,30,0	script	Priest Master	NPC_PLAYER,{
    /*
    if (!is_staff())
        goto L_Close;
    */
    if (!(MAGIC_SUBCLASS & CL_PRIEST))
        goto L_SignUp;
    goto L_Member;

// Sign Up
L_SignUp:
    // Not allowed if subclass filled or not from main class
    if (total_subclass() >= max_subclass() || getskilllv(WIZARD_MAGE) < 2)
        goto L_Close;
    mesn;
    mesq l("Hey there! Do you want to join the Priest Class?");
    mesc l("Warning: If you join a subclass, you can't leave it later!"), 1;
    next;
    if (askyesno() != ASK_YES)
        close;
    // TODO: Requeriment for signing up to a subclass? Or is the tier + skill quest hard enough?
    MAGIC_SUBCLASS=MAGIC_SUBCLASS|CL_PRIEST;
    mesn;
    mesq l("Welcome to the Priest guild!");
    close;

// Close
L_Close:
    goodbye;
    closedialog;
    close;

L_Missing:
    mesn;
    mesq l("Hey hey! You don't have that stuff, CAN'T YOU READ?!");
    percentheal 0, -10;
    next;
    goto L_Member;

// Membership area
// Priest
// AL_HOLYLIGHT (standard holy magic attack - 125% of MATK)
// MG_SRECOVERY (small increase to SP Recovery while idle each 10 sec.)
// ALL_RESURRECTION (Revive at 10% HP. May insta-kill undead. Max Level 3~4)
// PR_ASPERSIO (Change anyone weapon to holy for 60s. Or 40 defense-disregard holy dmg to undead/evil)
// TF_DETOXIFY (cancels poison. 40% MP.)

L_Member:
    mesn;
    mesq l("Hey there! Do you want to learn new skills for a very small teaching fee?");
    select
        rif(sk_intcost(AL_HOLYLIGHT) && !getskilllv(AL_HOLYLIGHT), l("Learn Holy Light")),
        rif(sk_intcost(TF_DETOXIFY) && !getskilllv(TF_DETOXIFY), l("Learn Detoxify")),
        rif(sk_intcost(ALL_RESURRECTION) && getskilllv(ALL_RESURRECTION) < (1+degree_subclass()/2), l("Learn Ressurection")),
        rif(sk_intcost(MG_SRECOVERY) && sk_canlvup(MG_SRECOVERY), l("Improve Mana Recovery")),
        rif(sk_intcost(PR_ASPERSIO) && sk_canlvup(PR_ASPERSIO), l("Improve Blessed Weapon")),
        l("Leave Subclass"),
        l("Nothing at the moment.");
    mes "";
    switch (@menu) {
        case 1:
            mesc l("[Holy Light]");
            mesc l("The basic magic attack from a Priest. No effect against Holy Monsters.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(MoubooFigurine),   (getskilllv(AL_HOLYLIGHT)+1)*3,  getitemlink(MoubooFigurine));
            mesc l("@@/@@ @@", countitem(GemPowder),        (getskilllv(AL_HOLYLIGHT)+1)*1,  getitemlink(GemPowder));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(MoubooFigurine)    < (getskilllv(AL_HOLYLIGHT)+1)*3 ||
                    countitem(GemPowder)         < (getskilllv(AL_HOLYLIGHT)+1)*1) goto L_Missing;

                delitem MoubooFigurine,      (getskilllv(AL_HOLYLIGHT)+1)*3;
                delitem GemPowder,           (getskilllv(AL_HOLYLIGHT)+1)*1;

                sk_lvup(AL_HOLYLIGHT);

                next;
            }
            break;
        case 2:
            mesc l("[Detoxify]");
            mesc l("Cancels Poison. Antidotes are hard to come by, anyway.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(Candy),         (getskilllv(TF_DETOXIFY)+1)*70, getitemlink(Candy));
            mesc l("@@/@@ @@", countitem(Root),          (getskilllv(TF_DETOXIFY)+1)*10, getitemlink(Root));
            mesc l("@@/@@ @@", countitem(Mashmallow),    (getskilllv(TF_DETOXIFY)+1)*2,  getitemlink(Mashmallow));
            mesc l("@@/@@ @@", countitem(FluoPowder),    (getskilllv(TF_DETOXIFY)+1)*1,  getitemlink(FluoPowder));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(Candy)            < (getskilllv(TF_DETOXIFY)+1)*70 ||
                    countitem(Root)             < (getskilllv(TF_DETOXIFY)+1)*10 ||
                    countitem(Mashmallow)       < (getskilllv(TF_DETOXIFY)+1)*2 ||
                    countitem(FluoPowder)       < (getskilllv(TF_DETOXIFY)+1)*1) goto L_Missing;

                delitem Candy,          (getskilllv(TF_DETOXIFY)+1)*70;
                delitem Root,           (getskilllv(TF_DETOXIFY)+1)*10;
                delitem Mashmallow,     (getskilllv(TF_DETOXIFY)+1)*2;
                delitem FluoPowder,     (getskilllv(TF_DETOXIFY)+1)*1;

                sk_lvup(TF_DETOXIFY);

                next;
            }
            break;
        case 3:
            mesc l("[Ressurection]");
            mesc l("Raise fallen party members so they can keep fighting.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(Acorn),            (getskilllv(ALL_RESURRECTION)+1)*60, getitemlink(Acorn));
            mesc l("@@/@@ @@", countitem(Bread),            (getskilllv(ALL_RESURRECTION)+1)*20, getitemlink(Bread));
            mesc l("@@/@@ @@", countitem(SmallMushroom),    (getskilllv(ALL_RESURRECTION)+1)*20, getitemlink(SmallMushroom));
            mesc l("@@/@@ @@", countitem(PinkBlobime),      (getskilllv(ALL_RESURRECTION)+1)*15,  getitemlink(PinkBlobime));
            mesc l("@@/@@ @@", countitem(Pearl),            (getskilllv(ALL_RESURRECTION)+1)*2,  getitemlink(Pearl));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(Acorn)         < (getskilllv(ALL_RESURRECTION)+1)*60 ||
                    countitem(Bread)         < (getskilllv(ALL_RESURRECTION)+1)*20 ||
                    countitem(SmallMushroom) < (getskilllv(ALL_RESURRECTION)+1)*15 ||
                    countitem(PinkBlobime)   < (getskilllv(ALL_RESURRECTION)+1)*20 ||
                    countitem(Pearl)         < (getskilllv(ALL_RESURRECTION)+1)*2) goto L_Missing;

                delitem Acorn,          (getskilllv(ALL_RESURRECTION)+1)*60;
                delitem Bread,          (getskilllv(ALL_RESURRECTION)+1)*20;
                delitem SmallMushroom,  (getskilllv(ALL_RESURRECTION)+1)*20;
                delitem PinkBlobime,    (getskilllv(ALL_RESURRECTION)+1)*15;
                delitem Pearl,          (getskilllv(ALL_RESURRECTION)+1)*2;

                sk_lvup(ALL_RESURRECTION);

                next;
            }
            break;
        case 4:
            mesc l("[MP Regen]");
            mesc l("Heals more mana while standing.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(Acorn),         (getskilllv(MG_SRECOVERY)+1)*70, getitemlink(Acorn));
            mesc l("@@/@@ @@", countitem(Root),          (getskilllv(MG_SRECOVERY)+1)*10, getitemlink(Root));
            mesc l("@@/@@ @@", countitem(FrozenYetiTear),(getskilllv(MG_SRECOVERY)+1)*2,  getitemlink(FrozenYetiTear));
            mesc l("@@/@@ @@", countitem(FluoPowder),    (getskilllv(MG_SRECOVERY)+1)*1,  getitemlink(FluoPowder));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(Acorn)            < (getskilllv(MG_SRECOVERY)+1)*70 ||
                    countitem(Root)             < (getskilllv(MG_SRECOVERY)+1)*10 ||
                    countitem(FrozenYetiTear)   < (getskilllv(MG_SRECOVERY)+1)*2 ||
                    countitem(FluoPowder)       < (getskilllv(MG_SRECOVERY)+1)*1) goto L_Missing;

                delitem Acorn,          (getskilllv(MG_SRECOVERY)+1)*70;
                delitem Root,           (getskilllv(MG_SRECOVERY)+1)*10;
                delitem FrozenYetiTear, (getskilllv(MG_SRECOVERY)+1)*2;
                delitem FluoPowder,     (getskilllv(MG_SRECOVERY)+1)*1;

                sk_lvup(MG_SRECOVERY);

                next;
            }
            break;
        case 5:
            mesc l("[Bless Weapon]");
            mesc l("Changes anyone's weapon to Holy elemental. Causes small damage when used on offensive.");
            mes "";
            mesn;
            mesq l("This useful skill will only require:");
            mesc l("@@/@@ @@", countitem(Acorn),         (getskilllv(PR_ASPERSIO)+1)*70, getitemlink(Acorn));
            mesc l("@@/@@ @@", countitem(Root),          (getskilllv(PR_ASPERSIO)+1)*10, getitemlink(Root));
            mesc l("@@/@@ @@", countitem(FrozenYetiTear),(getskilllv(PR_ASPERSIO)+1)*2,  getitemlink(FrozenYetiTear));
            mesc l("@@/@@ @@", countitem(FluoPowder),    (getskilllv(PR_ASPERSIO)+1)*1,  getitemlink(FluoPowder));
            next;
            if (askyesno() == ASK_YES) {
                if (
                    countitem(Acorn)            < (getskilllv(PR_ASPERSIO)+1)*70 ||
                    countitem(Root)             < (getskilllv(PR_ASPERSIO)+1)*10 ||
                    countitem(FrozenYetiTear)   < (getskilllv(PR_ASPERSIO)+1)*2 ||
                    countitem(FluoPowder)       < (getskilllv(PR_ASPERSIO)+1)*1) goto L_Missing;

                delitem Acorn,          (getskilllv(PR_ASPERSIO)+1)*70;
                delitem Root,           (getskilllv(PR_ASPERSIO)+1)*10;
                delitem FrozenYetiTear, (getskilllv(PR_ASPERSIO)+1)*2;
                delitem FluoPowder,     (getskilllv(PR_ASPERSIO)+1)*1;

                sk_lvup(PR_ASPERSIO);

                next;
            }
            break;
        case 6:
            // All skills related may include the basic class skills if they're related.
            mesc l("WARNING: If you leave the subclass, you'll lose all skills related to it!"), 1;
            mesc l("This cannot be undone. Are you sure?"), 1;
            mes "";
            if (askyesno() == ASK_YES) {
                mes "";
                if (validatepin()) {
                    skill AL_HOLYLIGHT, 0, 0;
                    skill TF_DETOXIFY, 0, 0;
                    skill ALL_RESURRECTION, 0, 0;
                    skill MG_SRECOVERY, 0, 0;
                    skill PR_ASPERSIO, 0, 0;
                    MAGIC_SUBCLASS=MAGIC_SUBCLASS^CL_PRIEST;
                    mesc l("You abandoned the PRIEST class!"), 1;
                    close;
                } else {
                    mesc l("Failed to validate pin. Aborting.");
                    next;
                }
            } else {
                mes "";
                mesc l("Operation aborted. Phew!");
                next;
            }
            break;
        default:
            goto L_Close;
    }

    goto L_Member;

OnInit:
    .@npcId = getnpcid(0, .name$);
    //setunitdata(.@npcId, UDT_HEADTOP, WarlordHelmet); // TODO: wizard hat
    setunitdata(.@npcId, UDT_HEADMIDDLE, SorcererRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, JeansChaps);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;
}

