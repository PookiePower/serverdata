// TMW2 Script
// Modified by Jesusalva

// Evol scripts.
// Authors:
//    4144
//    Qwerty Dragon
//    Reid
//    Vasily_Makarov
//    Jesusalva
// Description:
//    Starting script of Evol Online, modified for TMW2.
// Translation:
//      FR Translated 

000-0,23,20,0	script	Sailors	NPC_SAILORS,6,6,{

OnTouch:
    //checkclientversion;
    .@lang = requestlang();
    if (.@lang >= 0 && .@lang <= 9) Lang = .@lang;

    showavatar 3;

    mesn "Narrator";
    mesc l("You open your eyes. The salt water is not helping your vision.");
    mesc l("(Click next button to advance dialogs)");
    next;
    mesc l("A blur shape forms in your front. It seems to be a large ship.");
    next;
    mesc l("Wait. A ship? Where are you, after all?");
    next;
    mesc l("Your body aches. You can't remember what happened. But this is not important now. The ships sailors are shouting at you!");
    next;

    showavatar NPC_ORC_MAN;
    setcamnpc "Sailors", -64, -32;
    mesn "Orc Voice";
    mesq lg("Hey kid! Can you hear me?");
    next;

    showavatar NPC_SILVIO;
    setcamnpc "Sailors", 0, -32;
    mesn "Human Voice";
    mesq lg("Hear you? How do you even know she can understand you?!", "Hear you? How do you even know he can understand you?!");
    next;

    showavatar NPC_TRITAN_MAN_SAILOR;
    setcamnpc "Sailors", 144, -80;
    mesn "Tritan Voice";
    mesq lg("We speak various languages on this world. Let's try gesturing!");
    next;

    showavatar NPC_RAIJIN_MAN;
    setcamnpc;
    mesn "Raijin Voice";
    mesq l("Hey you! Can you hear us? Are you okay?");
    next;
    restorecam;

    asklanguage(LANG_ON_SEA);

    showavatar NPC_SAILORS;
    setcamnpc;
    mes "";
    mesn;
    mesq lg("Oh, she's still alive!", "Oh, he's still alive!");
    next;
    showavatar NPC_TRITAN_MAN_SAILOR;
    setcamnpc "Sailors", 144, -80;
    mesq lg("This girl needs help, we need to rescue her!", "This guy needs help, we need to rescue him!");
    next;
    restorecam;

    showavatar;
    mesn "Narrator";
    mesc l("The sailors take you aboard their ship.");
    mesc l("You faint from the pain, but at least now, you are in good hands.");
    next;
    mesc l("Click on the NPCs (Non-Player Characters) around you to continue the introduction.");
    next;

    addtimer(15000,"Elmo#sailors::OnSlow");
    warp "000-0-0", 26, 28;

    closedialog;
    close;

OnInit:
    .sex = G_MALE;
    end;
}
