// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Special Soul Menhir which only allows leaving the map.

001-4,152,154,0	script	Soul Menhir#001-4	NPC_SOUL_CLEAN,{
    mesn;
    mes l("(A mystical aura surrounds this stone. It probably can return you home. What do you do?)");

    menu
        l("Touch it."), L_Warp,
        l("Leave it alone."), -;
    close;

L_Warp:
    warp getsavepoint(0), getsavepoint(1), getsavepoint(2);
    close;

}

