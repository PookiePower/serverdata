// TMW2 Script
// Author:
//  Jesusalva, Saulc
// Description:
//  This GM NPC controls spawns and item drops on Aeros
// Monsters are sorted alphabetically, except bif

001-1,250,20,0	script	Mana Being#001-1	NPC_ALIGE_OUTSIDE_BARREL,{
    function spawner { // (memo, ID, amount)
        // First argument is a memorand for Script Writers, usually name. It must be present, but can have whatever value you want. (Unused)

        // [0] East [1] West [2] Full
        switch($@AEROS_SPWN) {
        case 1:
            areamonster("001-1", 20, 20, 140, 140, strmobinfo(1, getarg(1)), getarg(1), getarg(2), "Mana Being#001-1::OnAerosMobDeath");
            break;
        case 2:
            areamonster("001-1", 20, 20, 340, 160, strmobinfo(1, getarg(1)), getarg(1), getarg(2), "Mana Being#001-1::OnAerosMobDeath");
            break;
        default:
            areamonster("001-1", 171, 320, 158, 340, strmobinfo(1, getarg(1)), getarg(1), getarg(2), "Mana Being#001-1::OnAerosMobDeath");
            break;
        }
    }

    function mkitem { // ( ID{, Amount} )
        // [0] East [1] West [2] Full makeitem
        switch($@AEROS_SPWN) {
        case 1:
            for (.@i = 0; .@i < getarg(1,1); .@i++)
                makeitem(getarg(0), 1, "001-1", rand(20,140), rand(20,140));
            break;
        case 2:
            for (.@i = 0; .@i < getarg(1,1); .@i++)
                makeitem(getarg(0), 1, "001-1", rand(20,340), rand(20,160));
            break;
        default:
            for (.@i = 0; .@i < getarg(1,1); .@i++)
                makeitem(getarg(0), 1, "001-1", rand(171,320), rand(158,340));
            break;
        }
    }

    function buryitem { // ( ID{, Amount} )
        // [0] East [1] West [2] Full makeitem
        switch($@AEROS_SPWN) {
        case 1:
            shovel_scatter("001-1", 20, 20, 140, 140, getarg(0), getarg(1,1));
            break;
        case 2:
            shovel_scatter("001-1", 20, 20, 340, 160, getarg(0), getarg(1,1));
        default:
            shovel_scatter("001-1",171,158, 320, 340, getarg(0), getarg(1,1));
        }
    }

    .@curmobc=mobcount("001-1", "Mana Being#001-1::OnAerosMobDeath");
    if (!is_gm()) goto L_Unauthorized;

    @log_spawns=0;
    @log_mode=$@AEROS_SPWN;
    @log_ratio=$coinsrate;

    mesn;
    mes "Tired of walking the whole Aeros to spawn monsters, I was brought to existence.";
    mes "Monsters left: "+str(.@curmobc);

L_Menu:
    mes "";
    mes "Please select operation.";
    menu
        "Abort",L_Close,
        "Start/End Event", L_EventHandler,
        "Spawn",L_Spawn,
        rif(countitem(StrangeCoin) >= 10, "Drop stuff! (10x Coins)"),L_Drop,
        "Reconfigure spawn/warp points",L_Conf,
        rif(.WALL, "Open Extension"), L_DelWall,
        rif(!.WALL, "Close Extension"), L_AddWall,
        "Adjust coins drop rate",L_Rate;

L_Unauthorized:
    //dispbottom l("I am too far away to talk. Weird floating thingy...");
    dispbottom l("Monsters left: @@", .@curmobc);
    end;

L_Close:
    if (@log_spawns > 0) logmes "spawned "+str(@log_spawns)+" beigns at Aeros.", LOGMES_ATCOMMAND;
    if (@log_ratio != $coinsrate) logmes "set aeros ratio from "+str(@log_ratio)+" to "+str($coinsrate), LOGMES_ATCOMMAND;
    if (@log_mode == $@AEROS_SPWN) logmes "configured aeros spawn area to "+str($@AEROS_SPWN), LOGMES_ATCOMMAND;
    close;

L_Spawn:
    mes "";
    mes "Spawn from a preset (with intended levels) from this menu. Otherwise, use @aeros with same syntax.";
    mes "(agr) means Agressive Monsters on the set, DO NOT ABUSE.";
    next;
    menu
        "Abort", L_Menu,
        "Presets", L_Preset,
        "Normal", L_Norm,
        "Slime", L_Sli,
        "Plants", L_Plants,
        "Aggressive", L_Agr,
        "GM Event Only", L_EventOnly,
        "Boss", L_Boss;


L_Preset:
    select
        "Abort",                                                        // 1
        "20x Piou, Piousee, Ratto, 10x Croc",                           // 2
        "20x Little Blub, 10x Plushroom Field",                         // 3
        "(agr) 5x Tipiu, 10x Cave Maggot, 10x Bat",                    // 4
        "20x Scorpion, 10x Duck, 10x Maggot",                           // 5
        "10x Red Scorpion, 20x Fire Goblin, 5x Mana Ghost",             // 6
        "(agr) 1x Saxso Ghost, 20x House Maggot",                       // 7
        "(agr) 5x Slime Blast, 5x Red Slime, 10x White Slime",          // 8

        "(agr) 5x Mouboo, 4x Bandit, 2x Black Scorpion",                // 9
        "10x Giant Maggot, 10x Cave Snake, 10x Mana Bug",               // 10
        "1x Golden/Night Scorpion, 2x Santa Slime, 5x Copper Slime",    // 11
        "(agr) 2x Fallen Guards",                                       // 12

        "10x Clover Path, 5 groups of 5 random Bifs",                   // 13
        "5x Bifs, 4 groups of 5 random Bifs";                           // 14

    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        case 2:
            spawner(strmobinfo(1, Piou), Piou, 20);
            spawner(strmobinfo(1, Piousee), Piousee, 20);
            spawner(strmobinfo(1, Ratto), Ratto, 20);
            spawner(strmobinfo(1, Croc), Croc, 10);
            @log_spawns=@log_spawns+70;
            break;
        case 3:
            spawner(strmobinfo(1, LittleBlub), LittleBlub, 20);
            spawner(strmobinfo(1, PlushroomField), PlushroomField, 10);
            @log_spawns=@log_spawns+30;
            break;
        case 4:
            spawner(strmobinfo(1, Tipiu), Tipiu, 5);
            spawner(strmobinfo(1, CaveMaggot), CaveMaggot, 10);
            spawner(strmobinfo(1, Bat), Bat, 10);
            @log_spawns=@log_spawns+25;
            break;
        case 5:
            spawner(strmobinfo(1, Scorpion), Scorpion, 20);
            spawner(strmobinfo(1, Duck), Duck, 10);
            spawner(strmobinfo(1, Maggot), Maggot, 10);
            @log_spawns=@log_spawns+40;
            break;
        case 6:
            spawner(strmobinfo(1, RedScorpion), RedScorpion, 10);
            spawner(strmobinfo(1, ManaBug), FireGoblin, 20);
            spawner(strmobinfo(1, ManaGhost), ManaGhost, 5);
            @log_spawns=@log_spawns+35;
            break;
        case 7:
            spawner(strmobinfo(1, SaxsoGhost), SaxsoGhost, 1);
            spawner(strmobinfo(1, HouseMaggot), HouseMaggot, 20);
            @log_spawns=@log_spawns+21;
            break;
        case 8:
            spawner(strmobinfo(1, SlimeBlast), SlimeBlast, 5);
            spawner(strmobinfo(1, RedSlime), RedSlime, 5);
            spawner(strmobinfo(1, WhiteSlime), WhiteSlime, 10);
            @log_spawns=@log_spawns+20;
            break;

        case 9:
            spawner(strmobinfo(1, Mouboo), Mouboo, 5);
            spawner(strmobinfo(1, Bandit), Bandit, 4);
            spawner(strmobinfo(1, BlackScorpion), BlackScorpion, 2);
            @log_spawns=@log_spawns+11;
            break;
        case 10:
            spawner(strmobinfo(1, GiantMaggot), GiantMaggot, 10);
            spawner(strmobinfo(1, CaveSnake), CaveSnake, 10);
            spawner(strmobinfo(1, ManaBug), ManaBug, 10);
            @log_spawns=@log_spawns+30;
            break;
        case 11:
            spawner(strmobinfo(1, GoldenScorpion), GoldenScorpion, 1);
            spawner(strmobinfo(1, NightScorpion), NightScorpion, 1);
            spawner(strmobinfo(1, SantaSlime), SantaSlime, 5);
            spawner(strmobinfo(1, CopperSlime), CopperSlime, 10);
            @log_spawns=@log_spawns+17;
            break;
        case 12:
            spawner(strmobinfo(1, FallenGuard1), FallenGuard1, 1);
            spawner(strmobinfo(1, FallenGuard2), FallenGuard2, 1);
            @log_spawns=@log_spawns+2;
            break;

        //"10x Clover Path, 5 groups of 5 random Bifs",                   // 13
        //"5x Bifs, 4 groups of 5 random Bifs";                           // 14
        case 13:
            spawner(strmobinfo(1, CloverPatch), CloverPatch, 10);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            @log_spawns=@log_spawns+35;
            break;
        case 14:
            spawner(strmobinfo(1, Bif), Bif, 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            spawner(l("Mysterious Bif"), rand(1098,1118), 5);
            @log_spawns=@log_spawns+25;
            break;
    }

    if (@menu == 1) goto L_Spawn;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Preset;

L_Norm:
    select
        "Abort",                                // 1
        "Alpha Mouboo",                           // 2
        "Bat",                                  // 3
        "Bee",                                  // 4
        "Blub",                                 // 5
        "Candor Scorpion",                       // 6
        "Cave Snake",                            // 7
        "Crafty",                               // 8
        "Croc",                                 // 9
        "Desert Loghead",                        // 10
        "Desert Maggot",                         // 11
        "Duck",                                 // 12
        "Fire Goblin",                           // 13
        "Fluffy",                               // 14
        "Forest Mushroom",                       // 15
        "Little Blub",                           // 16
        "Giant Maggot",                          // 17
        "Golden Scorpion",                       // 18
        "Iced Fluffy",                           // 19
        "LogHead",                              // 20
        "Maggot",                               // 21
        "Magic Goblin",                          // 22
        "Mana Bug",                              // 23
        "Mana Ghost",                             // 24
        "Moggun",                               // 25
        "Mouboo",                                // 26
        "Ocean Croc",                            // 27
        "Pinkie",                               // 28
        "Piou",                                 // 29
        "Piousse",                              // 30
        "Poison Spiky Mushroom",                  // 31
        "Ratto",                                // 32
        "Red Butterfly",                         // 33
        "Red Mushroom",                          // 34
        "Red Scorpion",                          // 35
        "Scorpion",                             // 36
        "Silk Worm",                             // 37
        "Squirrel",                             // 38
        "Tipiou",                               // 39
        "Toppy Blub",                            // 40
        "Centaur",                              // 41
        "Mana Piou",                            // 42
        "Tortuga";//default

    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;


    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        case 2:
            spawner(("AlphaMouboo"), AlphaMouboo, .@c); break;
        case 3:
            spawner(("Bat"), Bat, .@c); break;
        case 4:
            spawner(("Bee"), Bee, .@c); break;
        case 5:
            spawner(("Blub"), Blub, .@c); break;
        case 6:
            spawner(("CandorScorpion"), CandorScorpion, .@c); break;
        case 7:
            spawner(("CaveSnake"), CaveSnake, .@c); break;
        case 8:
            spawner(("Crafty"), Crafty, .@c); break;
         case 9:
            spawner(("Croc"), Croc, .@c); break;
        case 10:
            spawner(("DesertLoghead"), DesertLogHead, .@c); break;
        case 11:
            spawner(("DesertMaggot"), DesertMaggot, .@c); break;
        case 12:
            spawner(("Duck"), Duck, .@c); break;
        case 13:
            spawner(("FireGoblin"), FireGoblin, .@c); break;
        case 14:
            spawner(("Fluffy"), Fluffy, .@c); break;
        case 15:
            spawner(("ForestMushroom"), ForestMushroom, .@c); break;
        case 16:
            spawner(("LittleBlub"), LittleBlub, .@c); break;
        case 17:
            spawner(("GiantMaggot"), GiantMaggot, .@c); break;
        case 18:
            spawner(("GoldenScorpion"), GoldenScorpion, .@c); break;
        case 19:
            spawner(("IcedFluffy"), IcedFluffy, .@c); break;
        case 20:
            spawner(("LogHead"), LogHead, .@c); break;
        case 21:
            spawner(("Maggot"), Maggot, .@c); break;
        case 22:
            spawner(("MagicGoblin"), MagicGoblin, .@c); break;
        case 23:
            spawner(("ManaBug"), ManaBug, .@c); break;
        case 24:
            spawner(("ManaGhost"), ManaGhost, .@c); break;
        case 25:
            spawner(("Moggun"), Moggun, .@c); break;
        case 26:
            spawner(("Mouboo"), Mouboo, .@c); break;
        case 27:
            spawner(("OceanCroc"), OceanCroc, .@c); break;
        case 28:
            spawner(("Pinkie"), Pinkie, .@c); break;
        case 29:
            spawner(("Piou"), Piou, .@c); break;
        case 30:
            spawner(("Piousse"), Piousse, .@c); break;
        case 31:
            spawner(("PoisonSpikyMushroom"), PoisonSpikyMushroom, .@c); break;
        case 32:
            spawner(("Ratto"), Ratto, .@c); break;
        case 33:
            spawner(("RedButterfly"), RedButterfly, .@c); break;
        case 34:
            spawner(("RedMushroom"), RedMushroom, .@c); break;
        case 35:
            spawner(("RedScorpion"), RedScorpion, .@c); break;
        case 36:
            spawner(("Scorpion"), Scorpion, .@c); break;
        case 37:
            spawner(("SilkWorm"), SilkWorm, .@c); break;
        case 38:
            spawner(("Squirrel"), Squirrel, .@c); break;
        case 39:
            spawner(("Tipiou"), Tipiou, .@c); break;
        case 40:
            spawner(("ToppyBlub"), ToppyBlub, .@c); break;
        case 41:
            spawner(("Centaur"), Centaur, .@c); break;
        case 42:
            spawner(("ManaPiou"), ManaPiou, .@c); break;
        default:
            spawner(("Tortuga"), Tortuga, .@c); break;
    }

    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Norm;

L_Sli:
    select
        "Abort",                                        // 1
        "Azul Slime",                                    // 2
        "Blue Slime",                                    // 3
        "Candied Slime",                                 // 4
        "Copper Slime",                                  // 5
        "Green Slime",                                   // 6
        "Lava Slime",                                    // 7
        "Rudolph Slime",                                  // 8
        "Red Slime",                                     // 9
        "Santa Slime",                                   // 10
        "Sea Slime",                                     // 11
        "Slime Blast",                                   // 12
        "White Slime",                                   // 13
        "Yellow Slime";                                  // 14

    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;


    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        case 2:
            spawner(("AzulSlime"), AzulSlime, .@c); break;
        case 3:
            spawner(("BlueSlime"), BlueSlime, .@c); break;
        case 4:
            spawner(("CandiedSlime"), CandiedSlime, .@c); break;
        case 5:
            spawner(("CopperSlime"), CopperSlime, .@c); break;
        case 6:
            spawner(("GreenSlime"), GreenSlime, .@c); break;
        case 7:
            spawner(("LavaSlime"), LavaSlime, .@c); break;
        case 8:
            spawner(("RudolphSlime"), RudolphSlime, .@c); break;
        case 9:
            spawner(("RedSlime"), RedSlime, .@c); break;
        case 10:
            spawner(("SantaSlime"), SantaSlime, .@c); break;
        case 11:
            spawner(("SeaSlime"), SeaSlime, .@c); break;
        case 12:
            spawner(("SlimeBlast"), SlimeBlast, .@c); break;
        case 13:
            spawner(("WhiteSlime"), WhiteSlime, .@c); break;
        case 14:
            spawner(("YellowSlime"), YellowSlime, .@c); break;

        }

    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Sli;


L_Plants:
    select
        "Abort",                                // 1
        "Chagashroom Field",                     // 2
        "Clover Patch",                          // 3
        "Cobalt Plant",                          // 4
        "Dummy",                                // 5
        "Gamboge Plant",                          // 6
        "Manana Tree",                           // 7
        "Mauve Plant",                           // 8
        "Plushroom Field",                       // 9
        "Small Ruby Bif",                         // 10
        "Small Topaz Bif",                        // 11
        "Small Emerald Bif",                      // 12
        "Small Diamond Bif",                      // 13
        "Small Amethyst Bif",                     // 14
        "Small Sapphire Bif",                     // 15
        "Ruby Bif",                              // 16
        "Topaz  Bif",                             // 17
        "Emerald Bif",                           // 18
        "Diamond Bif",                           // 19
        "Amethyst Bif",                          // 20
        "Sapphire Bif",                          // 21
        "Big Ruby Bif",                            // 22
        "Big Topaz Bif",                          // 23
        "Big Emerald Bif",                        // 24
        "Big Diamond Bif",                        // 25
        "Big Amethyst Bif",                       // 26
        "Big Sapphire Bif";                      // 27

    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;


    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        case 2:
            spawner(("ChagashroomField"), ChagashroomField, .@c); break;
        case 3:
            spawner(("CloverPatch"), CloverPatch, .@c); break;
        case 4:
            spawner(("CobaltPlant"), CobaltPlant, .@c); break;
        case 5:
            spawner(("Dummy"), Dummy, .@c); break;
        case 6:
            spawner(("GambogePlant"), GambogePlant, .@c); break;
        case 7:
            spawner(("MananaTree"), MananaTree, .@c); break;
        case 8:
            spawner(("MauvePlant"), MauvePlant, .@c); break;
        case 9:
            spawner(("PlushroomField"), PlushroomField, .@c); break;
        case 10:
            spawner(("SmallRubyBif"), SmallRubyBif, .@c); break;
        case 11:
            spawner(("SmallTopazBif"), SmallTopazBif, .@c); break;
        case 12:
            spawner(("SmallEmeraldBif"), SmallEmeraldBif, .@c); break;
        case 13:
            spawner(("SmallDiamondBif"), SmallDiamondBif, .@c); break;
        case 14:
            spawner(("SmallAmethystBif"), SmallAmethystBif, .@c); break;
        case 15:
            spawner(("SmallSapphireBif"), SmallSapphireBif, .@c); break;
        case 16:
            spawner(("RubyBif"), RubyBif, .@c); break;
        case 17:
            spawner(("TopazBif"), TopazBif, .@c); break;
        case 18:
            spawner(("EmeraldBif"), EmeraldBif, .@c); break;
        case 19:
            spawner(("DiamondBif"), DiamondBif, .@c); break;
        case 20:
            spawner(("AmethystBif"), AmethystBif, .@c); break;
        case 21:
            spawner(("SapphireBif"), SapphireBif, .@c); break;
        case 22:
            spawner(("BigRubyBif"), BigRubyBif, .@c); break;
        case 23:
            spawner(("BigTopazBif"), BigTopazBif, .@c); break;
        case 24:
            spawner(("BigEmeraldBif"), BigEmeraldBif, .@c); break;
        case 25:
            spawner(("BigDiamondBif"), BigDiamondBif, .@c); break;
        case 26:
            spawner(("BigAmethystBif"), BigAmethystBif, .@c); break;
        case 27:
            spawner(("BigSapphireBif"), BigSapphireBif, .@c); break;

            }

    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Plants;


L_Agr:
    select
        "Abort",                                    // 1
        "Angry Red Scorpion",                         // 2
        "Angry Scorpion",                            // 3
        "Bandit",                                   // 4
        "Black Scorpion",                            // 5
        "Cave Maggot",                               // 6
        "Robin Bandit",                                  // 7
        "Dark Lizard",                               // 8
        "Desert Bandit",                             // 9
        "Frista Guard",                              // 10
        "Forain",                                   // 11
        "Halin Guard",                             // 12
        "House Maggot",                               // 13
        "Magic Goblin",                              // 14
        "Mountain Snake",                            // 15
        "Ocean Pirate",                              // 16
        "Sarracenus",                               // 17
        "Skeleton",                                 // 18
        "Snake",                                    // 19
        "Tipiu",                                    // 20
        "Vampire Bat",                               // 21
        "Wolvern",                                  // 22
        "Terranite",                                 // 23
        "Yeti"; // Default


    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;


    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        case 2:
            spawner(("AngryRedScorpion"), AngryRedScorpion, .@c); break;
        case 3:
            spawner(("AngryScorpion"), AngryScorpion, .@c); break;
        case 4:
            spawner(("Bandit"), Bandit, .@c); break;
        case 5:
            spawner(("BlackScorpion"), BlackScorpion, .@c); break;
        case 6:
            spawner(("CaveMaggot"), CaveMaggot, .@c); break;
        case 7:
            spawner(("RobinBandit"), RobinBandit, .@c); break;
        case 8:
            spawner(("DarkLizard"), DarkLizard, .@c); break;
        case 9:
            spawner(("DesertBandit"), DesertBandit, .@c); break;
        case 10:
            spawner(("Frostia Guard"), FallenGuard1, .@c); break;
        case 11:
            spawner(("Forain"), Forain, .@c); break;
        case 12:
            spawner(("Halin Guard"), FallenGuard2, .@c); break;
        case 13:
            spawner(("HouseMaggot"), HouseMaggot, .@c); break;
        case 14:
            spawner(("MagicGoblin"), MagicGoblin, .@c); break;
        case 15:
            spawner(("MountainSnake"), MountainSnake, .@c); break;
        case 16:
            spawner(("OceanPirate"), OceanPirate, .@c); break;
        case 17:
            spawner(("Sarracenus"), Sarracenus, .@c); break;
        case 18:
            spawner(("Skeleton"), Skeleton, .@c); break;
        case 19:
            spawner(("Snake"), Snake, .@c); break;
        case 20:
            spawner(("Tipiu"), Tipiu, .@c); break;
        case 21:
            spawner(("VampireBat"), VampireBat, .@c); break;
        case 22:
            spawner(("Wolvern"), Wolvern, .@c); break;
        case 23:
            spawner(("Terranite"), Terranite, .@c); break;
        default:
            spawner(("Yeti"), Yeti, .@c); break;

            }

    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Agr;









L_EventOnly:
    mesc l("WARNING: Spawn these monsters with moderation!"), 1;
    mesc l("Some of them have FORBIDDEN DROPS, which will be DELETED when their event starts!"), 1;
    mes "";
    select
        "Abort",                                                  // 1
        "Bhop Fluffy",                                            // 2
        "Easter Mouboo",                                          // 3
        "Evil Scythe",                                            // 4
        "Jack'O",                                                 // 5
        "Magic Ratto",                                            // 6
        "Moonshroom",                                             // 7
        "Small Magic Bif",                                        // 8
        "Magic Bif",                                              // 9
        "Bronze Chest",                                           // 10
        "Bronze Mimic",                                           // 11
        "Silver Chest",                                           // 12
        "Silver Mimic",                                           // 13
        "Evil Chest",                                             // 14
        "Big Magic Bif"; // default


    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        case 2:
            spawner(("BhopFluffy"), BhopFluffy, .@c); break;
        case 3:
            spawner(("EasterMouboo"), EasterMouboo, .@c); break;
        case 4:
            spawner(("EvilScythe"), EvilScythe, .@c); break;
        case 5:
            spawner(("Jack'O"), JackO, .@c); // WUT
        case 6:
            spawner(("MagicRatto"), MagicRatto, .@c); break;
        case 7:
            spawner(("Moonshroom"), Moonshroom, .@c); // WUT
        case 8:
            spawner(("SmallMagicBif"), SmallMagicBif, .@c); break;
        case 9:
            spawner(("MagicBif"), MagicBif, .@c); break;
        case 10:
            spawner(("BronzeChest"), BronzeChest, .@c); break;
        case 11:
            spawner(("BronzeMimic"), BronzeMimic, .@c); break;
        case 12:
            spawner(("SilverChest"), SilverChest, .@c); break;
        case 13:
            spawner(("SilverMimic"), SilverMimic, .@c); break;
        case 14:
            spawner(("EvilChest"), EvilChest, .@c); break;
        default:
            spawner(("BigMagicBif"), BigMagicBif, .@c); break;
    }

    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_EventOnly;








L_Boss:
    select
        "Abort",                                          // 1
        "Giant Mutated Bat",                              // 2
        "Murderer Scorpion" ,                             // 3
        "Night Scorpion",                                 // 4
        "Saxso Ghost",                                    // 5
        "Bandit Lord",                                    // 6
        "Fafi Dragon",                                    // 67
        "The Yeti King"; // default

    if (@menu != 1)
        input .@c, 0, 100;
    if (.@c == 0 && @menu != 1)
        @menu=99;

    switch (@menu) {
        case 1:
            goto L_Spawn;
            break;
        case 2:
            spawner(("GiantMutatedBat"), GiantMutatedBat, .@c); break;
        case 3:
            spawner(("MurdererScorpion"), MurdererScorpion, .@c); break;
        case 4:
            spawner(("NightScorpion"), NightScorpion, .@c); break;
        case 5:
            spawner(("SaxsoGhost"), SaxsoGhost, .@c); break;
        case 6:
            spawner(("BanditLord"), BanditLord, .@c); break;
        case 7:
            spawner(("Fafi"), FafiDragon, .@c); break;
        default:
            spawner(("TheYetiKing"), TheYetiKing, .@c); break;

    }

    @log_spawns=@log_spawns+.@c;
    mes "";
    mes "Completed.";
    mes "Total spawns: "+str(@log_spawns);
    next;
    goto L_Boss;


L_Drop:
    select
        "Abort",                                   // 1
        "Bury 10x Coins",                          // 2
        "Bury Random Crap",                        // 3
        "Toothbrush",                              // 4
        "Bronze Gift",                             // 5
        "Silver Gift",                             // 6
        "Golden Gift",                             // 7
        "Prism Gift",                              // 8
        "Supreme Gift"; // default

    if (@menu != 1)
        input .@c, 0, countitem(StrangeCoin)/10;
    if (.@c == 0 && @menu != 1)
        @menu=1;

    delitem(StrangeCoin,.@c*10);
    switch (@menu) {
        case 1:
            goto L_Menu;
            break;
        case 2:
            buryitem(StrangeCoin, .@c*any(9,10,10,10,11,11)); break;
        case 3:
            setarray .@Treasure,
                                Bread,Candy,BugLeg,Cheese,Acorn,
                                ScorpionStinger,CoinBag,RustyKnife,TreasureKey,CasinoCoins,SulfurPowder,
                                Coal,IronOre,CopperOre,LeadOre,TinOre,SilverOre,GoldOre,PlatinumOre,IridiumOre,TitaniumOre,
                                Diamond,Ruby,Emerald,Sapphire,Topaz,Amethyst,
                                BoneArrow, SacredBullet;
            buryitem(.@Treasure, .@c*any(9,10,10,10,11,11)); break;
        case 4:
            mkitem(Toothbrush, .@c); break;
        case 5:
            mkitem(BronzeGift, .@c); break;
        case 6:
            mkitem(SilverGift, .@c); break;
        case 7:
            mkitem(GoldenGift, .@c); break;
        case 8:
            mkitem(PrismGift, .@c); break;
        default:
            mkitem(SupremeGift, .@c); break;

    }

    mes "";
    mes "Dropped "+.@c+" stuff.";
    next;
    goto L_Drop;

L_Rate:
    mes "";
    mes "Current drop rate: " + str($coinsrate);
    mes "Insert drop rate (from 0 to 10000, capped at 5000 or 50%). Use -1 to disable.";
    input $coinsrate;
    if ($coinsrate > 5000)
        $coinsrate=5000;
    mes "";
    mes "Ratio adjusted.";
    next;
    goto L_Menu;

L_EventHandler:
    if($@GM_EVENT) {
        announce ("The event is over!"), bc_all|bc_npc; // I haven't tested this yet.
        $@GM_EVENT=0;
    } else {
        announce ("The mana bridge to Aeros is open! To participate on event, talk to ##BSoul Menhir##b!"), bc_all|bc_npc;
        channelmes("#world", "An event is happening on Aeros! Hurry up!");
        $@GM_EVENT=1;
    }
    close;

L_AddWall:
    setcells "001-1", 169, 63, 169, 76, 3, "AerosWall";
    //setwall("001-1", 169, 63, 13, DOWN, false, "AerosWall");
    .WALL=1;
    close;

L_DelWall:
    delcells "AerosWall";
    //delwall("AerosWall");
    .WALL=0;
    close;

L_Conf:
    mes "";
    mesn;
    mesq l("Current Spawn Mode: "+$@AEROS_SPWN);
    next;
    select
        l("[0] East Aeros"),
        l("[1] West Aeros"),
        l("[2] Full Aeros");
    $@AEROS_SPWN=@menu-1;
    goto L_Menu;

OnAerosMobDeath:
    if ($coinsrate < 0) end;
    // A level 100 monster can have a 4% drop bonus against a level 0 monster.
    // $coinsrate acts stablishing a basic drop value. Advised value: 600 (up to 10% for normal mobs)
    if (rand(10000) <= $coinsrate + (atoi(strmobinfo(3,killedrid))*4))
        getmapxy(.@m$, .@x, .@y, 0);
        makeitem(StrangeCoin, 1, .@m$, .@x, .@y);
    end;

OnInit:
    if ($coinsrate == 0)
        $coinsrate=600; // Default value is 6% + mob str bonus
    end;
}



-	script	@aeros	32767,{
    end;

OnCall:
    if (!is_admin())
        end;
    if (getmapname() != "001-1") {
        dispbottom "This command can only be used at aeros.";
        end;
    }
    //.@atcmd_parameters$ = strtoupper(strip( // nah

    if (.@atcmd_numparameters != 2) {
        dispbottom "Use the numeric ID provided by the wiki. This is a safeguard to ensure you are not overkilling players.";
        dispbottom "Talk to Mana Being for a less fine-grained but much more optimized control over monsters.";
        dispbottom "This command takes exactly this syntax: Mob ID <space> amount.";
        end;
    }

        switch($@AEROS_SPWN) {
        case 1:
            areamonster("001-1", 20, 20, 140, 140, strmobinfo(1, atoi(.@atcmd_parameters$[0])), atoi(.@atcmd_parameters$[0]), atoi(.@atcmd_parameters$[1]), "Mana Being#001-1::OnAerosMobDeath");
            break;
        case 2:
            areamonster("001-1", 20, 20, 340, 160, strmobinfo(1, atoi(.@atcmd_parameters$[0])), atoi(.@atcmd_parameters$[0]), atoi(.@atcmd_parameters$[1]), "Mana Being#001-1::OnAerosMobDeath");
            break;
        default:
            areamonster("001-1", 171, 320, 158, 340, strmobinfo(1, atoi(.@atcmd_parameters$[0])), atoi(.@atcmd_parameters$[0]), atoi(.@atcmd_parameters$[1]), "Mana Being#001-1::OnAerosMobDeath");
            break;
        }
    logmes "@aeros "+strmobinfo(1, atoi(.@atcmd_parameters$[0])) + " " + .@atcmd_parameters$[1], LOGMES_ATCOMMAND;
    dispbottom "All monsters summoned.";

    end;

OnInit:
    bindatcmd "aeros", "@aeros::OnCall", 99, 99, 0;
}
