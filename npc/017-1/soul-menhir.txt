// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Soul Menhir

017-1,120,86,0	script	Soul Menhir#lof	NPC_SOUL_OLD,{
    @map$ = "017-1";
    setarray @Xs, 119, 120, 121, 119, 120, 121;
    setarray @Ys,  87,  88,  87,  88,  87,  88;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
