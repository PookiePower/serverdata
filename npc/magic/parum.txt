// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Magic Script: TMW2_PARUM
// 
// Attempts to transmutate Raw Logs.
// May create arrows, Mouboo Figurines, Warped Logs.
// With enough ManaExp may create WoodenLog.
// Is not powerful enough to create overly complex stuff like Wooden Sword, Wooden Shield or Wooden Bow
// Final item is random
// TODO: Using too much transmutation magic may have dire consequences! Like, uh, transmutating your head!
// Nah, it is probably just propaganda... I hope. ¬.¬

-	script	sk#parum	32767,{
    end;

/*
OnFriendlyDeath:
    emote 4;
    end;
*/

OnCall:
    // Other requeriments
    if (countitem(RawLog) < 1) {
        dispbottom l("You need @@ to cast this skill.", getitemlink(RawLog));
        end;
    }

    // Check cooldown
    if (@parum_at > gettimetick(2)) {
        dispbottom l("Skill is in cooldown for @@.", FuzzyTime(@parum_at));
        end;
    }

    // Check requisites
    if (!MagicCheck(TMW2_PARUM, 50))
        end;

    // Consume items
    delitem RawLog, 1;

    // Create the stuff based on MAGIC_EXP
    .@r=rand(1,41);
    if (.@r < 42-(MAGIC_EXP/2)) {
        getitem WarpedLog, 1;
    } else {
        if (.@r > 30+abizit())
            getitem MoubooFigurine, 1;
        else if (.@r > 20 && MAGIC_EXP > 82)
            getitem WoodenLog, 1;
        else
            getitem Arrow, .@r;
    }
    // I know, the code is not very sane. A number from 0 to 40 is cast.
    // You will get lots of useless Warped Logs until you have 82 MExp.
    // If you do not get a Warped Log, you have 25% chances of getting the
    // Mouboo figurine. The other will be arrows, unless you hit the 82 MExp value
    // which will add 25% chances to get a Wooden Log too. These values are estimate.

    // set cooldown
    @parum_at=gettimetick(2);
    @parum_at=@parum_at+4;

    // Get a few mana experience points (this is NOT used by Soul Menhir)
    GetManaExp(TMW2_PARUM, rand(1,3));
    end;

OnInit:
    bindatcmd "sk-parum", "sk#parum::OnCall", 0, 100, 0;
    end;
}
