function	script	SuperMenu	{
    do
    {
        clear;
        setnpcdialogtitle l("Super Menu");
        mes l("This menu contains all options available to you, based on your access privileges.");
        mes "";
        mes l("What do you want to access?");
        next;
        select
            l("Scheduled broadcasts"),
            rif(is_admin(), l("MOTD")),
            rif(is_gm(), l("Event management")),
            rif(is_admin() && !getcharid(2), l("Join teh Guild")),
            rif(is_admin(), l("Referral Program Report")),
            rif(is_admin(), "Broken checks"),
            rif(is_gm(), l("Seasonal Drop Control")),
            "Debug",
            "Quit";

        switch (@menu)
        {
            case 1: StoneBoard 1; break;
            case 2: MOTDConfig 1; break;
            case 3: GlobalEventMenu 1; break;
            case 4:
            	query_sql("UPDATE `char` SET `guild_id`=1 WHERE `char_id`="+getcharid(0));
                break;
            case 5: HallOfReferral; break;
            case 6: 
                delitem Aquada, 1;
                delitem Bread, 100;
                mes("Either delitem is not working, or you had 1 aquada and 100 bread.");
                next;
                break;
            case 7: SeasonControl; break;
            case 8: GlobalDebugMenu 1; break;
            default: close; break;
        }
    } while (1);
}



-	script	@super	32767,{
    end;

OnCall:

    if (!debug && !is_gm()) {
        dispbottom l("You do not have the required access privileges to use the Super Menu.");
        end;
    }

    SuperMenu;
    closedialog;
    end;

OnInit:
    bindatcmd "super", "@super::OnCall", 80, 99, 0;
    //bindatcmd "numa", "@super::OnCall", 80, 99, 0; // alias for those used to TMW's @numa - none at the moment, iirc
}
