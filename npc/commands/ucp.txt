function	script	UserCtrlPanel	{
    do
    {
        clear;
        setnpcdialogtitle l("User Control Panel");
        mes l("This menu gives you some options which affect your account.");
        mes l("In some cases, your pincode will be required.");
        mes "";
        mes l("What do you want to access?");
        next;
        select
            l("Rules"),
            l("Game News"),
            l("Account Information"),
            l("Change Language"),
            l("Quit");

        switch (@menu)
        {
            case 1: GameRules; break;
            case 2: GameNews; break;
            case 3:
                if (!validatepin())
                    break;
                if (!@lgc) {
                    query_sql("SELECT email,logincount,last_ip FROM `login` WHERE account_id="+getcharid(3)+" LIMIT 2", .@email$, .@lgc, .@ip$);
                    @email$=.@email$;
                    @lgc=.@lgc;
                    @ip$=.@ip$;
                } else {
                    .@email$=@email$;
                    .@lgc=@lgc;
                    .@ip$=@ip$;
                }
                mes l("Char Name: @@", strcharinfo(0));
                mes l("Party Name: @@", strcharinfo(1));
                mes l("Guild Name: @@", strcharinfo(2));
                mes l("Clan Name: @@", strcharinfo(4));
                mes "";
                mes l("Email: @@", .@email$[0]);
                if (Sex)
                    mes l("Male");
                else
                    mes l("Female");
                mes l("Last IP: @@", .@ip$[0]);
                mes l("Total Logins: @@", .@lgc[0]);
                next;
                if (@query)
                    break;
                @query=1;
                query_sql("SELECT name,last_login,last_map,partner_id FROM `char` WHERE account_id="+getcharid(3)+" LIMIT 9", .@name$, .@lastlogin$, .@map$, .@married);
                for (.@i = 1; .@i <= getarraysize(.@name$); .@i++) {
                mesn .@name$[.@i-1];
                mes l("Last Seen: @@", FuzzyTime(.@lastlogin$[.@i-1]));
                mes l("Last map: @@", .@map$[.@i-1]);
                if (.@married[.@i-1])
                    mes l("Married with @@", gf_charname(.@married[.@i-1]));
                mes "";
                }
                next;
                break;
            case 4: asklanguage(LANG_IN_SHIP); break;
            case 5: close; break;
        }
    } while (1);
}



-	script	@ucp	32767,{
    end;

OnCall:

    UserCtrlPanel;
    closedialog;
    end;

OnInit:
    bindatcmd "ucp", "@ucp::OnCall", 0, 99, 0;
}
