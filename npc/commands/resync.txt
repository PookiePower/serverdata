// TMW2 script
// Author: Jesusalva <admin@tmw2.org>
//
// Introduces @resync
// 
// It'll cast slide to your own position
// Hopefully making client update your real position without causing server warning


-	script	@resync	32767,{
    end;

OnCall:
    if (ispcdead()) {
        dispbottom l("Impossible to resync: You are dead.");
        end;
    }
    if (@rsync_delay > gettimetick(2) && readparam(Hp) < readparam(MaxHp)) {
        dispbottom l("Not resync'ing to prevent flood.");
        end;
    }
    getmapxy(.@m$, .@x, .@y, 0);
    slide .@x, .@y;
    @rsync_delay=gettimetick(2)+rand(6,15);
    end;

OnInit:
    bindatcmd "resync", "@resync::OnCall", 0, 60, 0;
    end;
}
