// Delete item ID on inventories, storages, guild storages and carts. Also affects mails.
// WARNING, irreversible and dangerous!
// DelItemFromEveryPlayer( ID )
function	script	DelItemFromEveryPlayer	{
    if (getarg(0, -1) < 0)
        return;

    query_sql("DELETE FROM `inventory` WHERE `nameid`="+getarg(0));
    query_sql("DELETE FROM `cart_inventory` WHERE `nameid`="+getarg(0));
    query_sql("DELETE FROM `storage` WHERE `nameid`="+getarg(0));
    query_sql("DELETE FROM `guild_storage` WHERE `nameid`="+getarg(0));
    query_sql("DELETE FROM `rodex_items` WHERE `nameid`="+getarg(0));
    query_sql("DELETE FROM `auction` WHERE `nameid`="+getarg(0));
    return;
}

function	script	GlobalEventMenu	{

    function rateManagement {
        clear;
        mes l("To get the current rate:");
        mes col("    @exprate", 7);
        next;
        mes l("To set the exp rate:");
        mes col("    @exprate ##Brate##b hours", 7);
        next;
        mes l("To reset back to normal:");
        mes col("    @exprate default", 7); // note to translators: any non-numerical value will reset so "default" is safe to translate
        next;
        return;
    }

    function dropManagement {
        clear;
        mes l("To get the current rate:");
        mes col("    @rates", 7);
        next;
        mes l("To set the drop rate:");
        mes col("    @droprate ##Brate##b hours", 7);
        next;
        mes l("To reset back to normal:");
        mes col("    @droprate default", 7); // note to translators: any non-numerical value will reset so "default" is safe to translate
        next;
        return;
    }

    function sEaster {
        // Delete all GoldenEasteregg and SilverEasteregg from every player
        // WARNING, possibly dangerous.
        DelItemFromEveryPlayer(GoldenEasteregg);
        DelItemFromEveryPlayer(GoldenEasteregg);

        // Enable event
        set $EVENT$, "Easter";
        logmes "Enabled EASTER event.", LOGMES_ATCOMMAND;
        return;
    }

    function sChristmas {
        // Delete all Christmas stuff?
        DelItemFromEveryPlayer(GoldenEasteregg);
        DelItemFromEveryPlayer(GoldenEasteregg);

        // Add drops?

        // Change maps?
        addmapmask "009-1", MASK_CHRISTMAS;
        addmapmask "017-2", MASK_CHRISTMAS;
        addmapmask "017-2-1", MASK_CHRISTMAS;
        addmapmask "017-3", MASK_CHRISTMAS;

        // Enable event
        set $EVENT$, "Christmas";
        logmes "Enabled CHRISTMAS event.", LOGMES_ATCOMMAND;
        return;
    }
    //MASK_CHRISTMAS



















    function seasonManagement {
        clear;
        mes l("Current event: @@", $EVENT$);
        menu
            l("Disable event"), -,
            l("Enable Easter"), -,
            l("Enable Worker's Day"), -,
            l("Reset Kill Saulc Event (Monthly)"), -,
            l("Enable Refeer Program"), -;

        switch (@menu) {
            case 1: set $EVENT$, "";$REFERRAL_ENABLED=0; logmes "Disabled events.", LOGMES_ATCOMMAND;
                break;
            case 2: sEaster(); break;
            case 3:
                set $EVENT$, "Worker Day";
                logmes "Enabled WORKERS DAY event.", LOGMES_ATCOMMAND;
                /*
                addmonsterdrop(Snake, Pearl, 10);
                debugmes "Snakes are now dropping Pearls.";
               */
                break;
            case 4: DelItemFromEveryPlayer(MurdererCrown); break;
            case 5: $EVENT$="Refeer";$REFERRAL_ENABLED=1; logmes "Enabled REFEER event.", LOGMES_ATCOMMAND;
                break;
        }

        return;
    }

    do
    {
        clear;
        setnpcdialogtitle l("Event Management");
        mes l("This menu allows you to manage events and gives access to event-related tools.");
        mes "";
        mes l("What do you want to access?");

        select
            l("Experience Rate management"),
            l("Drop Rate management"),
            l("Change Season Event"),
            rif(getarg(0,0), menuimage("actions/home", l("Return to Super Menu")));

        //.@c = getarg(0,0) ? 2 : 1; // 1 = back to event menu, 2 = back to super menu

        switch (@menu) {
            case 1: rateManagement; break;
            case 2: dropManagement; break;
            case 3: seasonManagement; break;
            default: return;
        }

    } while (true);
}



-	script	@event	32767,{
    end;

OnCall:
    if (!is_gm()) {
        end;
    }

    GlobalEventMenu;
    closedialog;
    end;
}

-	script	@toevent	32767,{
    end;

OnCall:
    if (!$@GM_EVENT)
        dispbottom l("The mana bridge is closed at the moment.");
    else if (BaseLevel < 10)
        dispbottom l("You are not strong enough to survive this trip.");
    else if (readparam(Sp) != readparam(MaxSp))
        dispbottom l("You need all your mana to do this trip.");
    else if (readparam(Hp) != readparam(MaxHp))
        dispbottom l("You cannot be hurt to do this trip.");
    else if (getmapname() ~= "001-*")
        dispbottom l("You are already at the Mana Plane of Existence.");
    else if (getmapname() == "boss" || getmapname() == "sec_pri" || getmapname() ~= "000-*" || getmapname() ~= "008-*" || getmapname() ~= "sore*")
        dispbottom l("The Mana Plane is currently out of reach.");
    else {
        .@gt=$@AEROS_SPWN;
        if (.@gt == 2)
            .@gt=rand(0,1);
        switch (.@gt) {
        case 0:
            warp "001-1", 235, 26; break;
        case 1:
            warp "001-1", 23, 108; break;
        }
        specialeffect(63, AREA, getcharid(3));
    }
    end;

OnInit:
    bindatcmd "event", "@event::OnCall", 99, 99, 0;
    bindatcmd "toevent", "@toevent::OnCall", 0, 99, 0;
}
