// @lang atcommand
// Changes Language
//
// group lv: 0
// group char lv: 0
// log: False
//
// usage:
//    @lang
//

-	script	@lang	32767,{
    end;

OnCall:
    checkclientversion;
    mesq l("Tell me which language you speak and I will change the note on the ship passenger list.");
    next;
    asklanguage(LANG_IN_SHIP);
    mes "";
    mesn;
    mesq l("Ok, done.");
    close;

OnInit:
    bindatcmd "lang", "@lang::OnCall", 0, 99, 0;
    end;
}
