// Author:
//    Saulc
//    Jesusalva

005-5,27,38,0	script	Nylo	NPC_PLAYER,{

    function nylo_thanks {
    	speech S_LAST_BLANK_LINE,
        l("You are very welcome.");
        close;
    }

    function nylo_items2 {
    	speech S_LAST_BLANK_LINE,
        	l("Let Cynric open your storage. You might also want to open your inventory."),
        	l("Pick an item from either storage or inventory and choose what you want to do with it by clicking the right button."),
        	l("You can also pick and drag items from one window into the other but this will move all items of this kind.");
        next;
        speech S_LAST_BLANK_LINE,
        	l("Items in your storage are totally safe. Banking itself is totally safe.");
        do
    	{
    		select
    		   	l("Thanks!");

        	switch (@menu)
        	{
            	case 1:
                	nylo_thanks;
                	break;
        	}
    	} while (@menu != 1);
    }

    function nylo_money2 {
    	speech S_LAST_BLANK_LINE,
        	l("That's easy. Cynric will save your money when you ask him to deposit. Or you pick up your money by asking to withdraw."),
        	l("You only have to tell him how much you want to deposit or withdraw."),
        	l("Also you can ask how much he already save for you by checking your balance.");
        next;
        speech S_LAST_BLANK_LINE,
        	l("Money in your storage is totally safe. Banking itself is totally safe.");

        do
    	{
    		select
            	l("Thanks!");

        	switch (@menu)
        	{
            	case 1:
                	nylo_thanks;
                	break;
        	}
    	} while (@menu != 1);
    }

    function nylo_items {
    	speech S_LAST_BLANK_LINE,
        	l("Let Cynric open your storage. You might also want to open your inventory."),
        	l("Pick an item from either storage or inventory and choose what you want to do with it by clicking the right button."),
        	l("You can also pick and drag items from one window into the other but this will move all items of this kind.");
        next;
        speech S_LAST_BLANK_LINE,
        	l("Items in your storage are totally safe. Banking itself is totally safe.");
        do
    	{
    		select
            	l("And how do I save money?"),
            	l("Thanks!");

        	switch (@menu)
        	{
            	case 1:
                	nylo_money2;
                	break;
            	case 2:
                	nylo_thanks;
                	break;
        	}
    	} while (@menu != 2);
    }

    function nylo_money {
    	speech S_LAST_BLANK_LINE,
        	l("That's easy. Cynric will save your money when you ask him to deposit. Or you pick up your money by asking to withdraw."),
        	l("You only have to tell him how much you want to deposit or withdraw."),
        	l("Also you can ask how much he already save for you by checking your balance.");
        next;
        speech S_LAST_BLANK_LINE,
        	l("Money in your storage is totally safe. Banking itself is totally safe.");

        do
    	{
    		select
            	l("And how do I save items?"),
            	l("Thanks!");

        	switch (@menu)
        	{
            	case 1:
                	nylo_items2;
                	break;
            	case 2:
                	nylo_thanks;
                	break;
        	}
    	} while (@menu != 2);
    }

	function nylo_explain {
		speech S_LAST_BLANK_LINE,
        	l("There are banks all over the world. Usually every important village or city has one."),
        	l("You can save both items and money at a bank.");

        do
    	{
    		select
            	l("How do I save items?"),
            	l("How do I save money?"),
            	l("Thanks!");

        	switch (@menu)
        	{
            	case 1:
                	nylo_items;
                	break;
            	case 2:
                	nylo_money;
                	break;
                case 3:
                	nylo_thanks;
                	break;
        	}
    	} while (@menu != 3);
	}

	speech S_LAST_BLANK_LINE,
        l("Visiting the bank too right? We're only a small village but Cynric is the best bank guy I know.");

    do
    {
    	select
            l("Yeah, you're right."),
            l("You store a large collection of goods. Could you perhaps sell me some?"),
            l("What is banking?");
        switch (@menu)
        {
            case 1:
                break;
            case 2:
                openshop "Nylo#Beer";
                break;
            case 3:
                nylo_explain;
                break;
        }
    } while (@menu != 1);

    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    //setunitdata(.@npcId, UDT_HEADMIDDLE, 1321); // Maybe Nylo could use the whole Candor set...
    setunitdata(.@npcId, UDT_HEADTOP, CreasedShirt);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CreasedShorts);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, CandorBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 26);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 0);

    .sex = G_MALE;
    .distance = 4;
    end;
}
005-5,27,38,0	trader	Nylo#Beer	NPC_HIDDEN,{

OnInit:
    tradertype(NST_MARKET);

    sellitem Beer, 350, 100;

    .sex = G_OTHER;
    .distance = 4;
    end;

OnClock0600:
OnClock1200:
OnClock1800:
OnClock0000:
    restoreshopitem Beer, 350, 100;

}
