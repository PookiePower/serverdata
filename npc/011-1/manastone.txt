// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  The last Mana Stone in the whole world, which is not owned by the Magic Council
//  or the Monster King.
//  It actually hates (or rather, fears) everybody, and is not always willing to
//  talk with people. May hide itself within the walls during these occasions.
//  It may not hate some very specific things which are lore-related.
//
//  Notes: During sieges, Monster King and Human Council apparitions, it may hide
//  itself.
//
//  Variables:
//      $MANA_BINT => Base Intelligence for Mana Stone
//      $MANA_BLVL => Base Level for Mana Stone
//      $MANA_JLVL => Base Job Level for Mana Stone
//      .int       => Int Increment
//      .lvl       => Lvl Increment
//      .jlvl      => Jlv Increment

011-1,0,0,0	script	Mana Stone	NPC_MANA_STONE,{

    if (BaseLevel < 40) goto L_NotWorthy;
    mesn;
    mes l("The mighty Mana Stone does not reacts against you.");
    mes l("Although this particular one seems to hate everyone and everything, it recognizes your strength.");
    mes l("If you fell ready, perhaps you should touch it?");
    mes "";
    menu
        l("Touch it!"), L_Level,
        l("Take it!"), L_NotWorthy2,
        l("Break it!"), L_NotWorthy2,
        l("Leave it alone!"), -;
    close;


L_NotWorthy:
    percentheal -70+BaseLevel, -100+BaseLevel;
    npctalk3 l("You are not worthy!");
    if (readparam(Sp) != readparam(MaxSp))
        dispbottom l("I must have full MP to touch it... Which I don't.");
    else
        dispbottom l("I should train my intelligence, and level up, both my base as my Job Level.");
    end;

L_Level:
    // See functions/util.txt for *mstone() details
    if (mstone(0)) goto L_LevelUp;
    if (mstone(1)) goto L_LevelUp;
    if (mstone(2)) goto L_LevelUp;
    if (mstone(3)) goto L_LevelUp;
    if (mstone(4)) goto L_LevelUp;
    if (mstone(5)) goto L_LevelUp;
    if (mstone(6)) goto L_LevelUp;
    if (MAGIC_LVL >= 7) npctalk3 l("You already got all power I could grant you!");
    if (is_gm()) percentheal -20, -50;
    if (MAGIC_LVL >= 7 || is_gm()) close;

L_NotWorthy2:
    if (is_gm()) movenpc(.name$, rand(200), rand(200));
    if (is_gm()) close;
    percentheal -20, -50;
    npctalk3 l("You are not worthy!");
    if (MAGIC_LVL == 0)
        dispbottom l("I should train my intelligence, have full MP, and don't neglect even Job Level.");
    end;

L_LevelUp:
    mes "";
    mes l("A great rush of mana flows though you.");
    if (!MAGIC_LVL) mes l("Magic Power is granted to you, but you die from it.");
    if (MAGIC_LVL) mes l("More Magic Power is granted to you, but you die from it.");
    MAGIC_LVL = MAGIC_LVL+1;
    // Magic Numbers work better
    getexp readparam(NextBaseExp)*771/1000, readparam(NextJobExp)*400/1000;
    // Non-magic numbers work bad
    //getexp readparam(NextBaseExp)*getbattleflag(death_penalty_base)/1000, readparam(NextJobExp)*getbattleflag(death_penalty_job)/1000;
    percentheal -100, -100;
    close;

OnInit:
    movenpc .name$, rand(200), rand(200);
    .sex = G_OTHER;
    .distance = 6;
    end;

OnClock0030:
OnClock0120:
OnClock0210:
OnClock0300:
OnClock0450:
OnClock0540:
OnClock0630:
OnClock0720:
OnClock0810:
OnClock0900:
OnClock1050:
OnClock1140:
OnClock1230:
OnClock1320:
OnClock1410:
OnClock1500:
OnClock1650:
OnClock1740:
OnClock1830:
OnClock1920:
OnClock2010:
OnClock2100:
OnClock2250:
OnClock2340:
    npctalk "Those who are worthy, may get my magic. For the others, death awaits!";
    movenpc .name$, rand(200), rand(200);
    end;

}
