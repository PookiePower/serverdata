// Evol functions.
// Author:
//    4144
//    Jesusalva
// Description:
//    Shops utility functions
// Variables:
//    Item ID, Item Price, Item Amount

function	script	restoreshopitem	{
    if (getarg(2, -1337) == -1337) {
        .@amount=getarg(1);
        .@price=-1;
    } else {
        .@amount=getarg(2);
        .@price=getarg(1);
    }

    if (shopcount(getarg(0)) < .@amount)
        sellitem getarg(0), .@price, .@amount;
    return;
}
