// Shake player screen
// Usage:
// set @max_shake then call OnShake
// call OnGM

-	script	shake	32767,{
    end;

// Loops until @shake is @max_shake
OnShake:
    @shake+=1;
    movecam rand(-20,20), rand(-20,20);
    //closeclientdialog;

    if (@shake < @max_shake) {
        addtimer(50, "shake::OnShake");
    } else {
        @shake=0;
        restorecam;
        closedialog;
    }
    end;

// Called by GM Command
OnGM:
    @shake=0;
    @max_shake=rand(6,10);
    addtimer(50, "shake::OnShake");
    end;

OnCall:
    getmapxy(.@m$,.@x,.@y,0);
    areatimer(.@m$, .@x-15, .@y-15, .@x+15, .@y+15, 10, "shake::OnGM");
    end;

OnInit:
    bindatcmd "shake", "shake::OnCall", 80, 80, 1;
    end;
}
