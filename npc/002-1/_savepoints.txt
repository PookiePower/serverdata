// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 002-1: Second Deck saves
002-1,55,40,0	script	#save_002-1_55_40	NPC_SAVE_POINT,{
    savepointparticle .map$, .x, .y, NO_INN;
    close;

OnInit:
    .distance = 2;
    .sex = G_OTHER;
    end;
}
