// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Forge firearms
017-4,34,23,0	script	Pyndragon	NPC_PLAYER,{
    // craft_gun( BaseItem1, Amount, BaseItem2, Amount, BaseItem3, Amount, PrizeItem, Price )
    function craft_gun {
        .@base1=getarg(0);
        .@amon1=getarg(1);
        .@base2=getarg(2);
        .@amon2=getarg(3);
        .@base3=getarg(4);
        .@amon3=getarg(5);
        .@prize=getarg(6);
        .@price=getarg(7);

        mesn;
        mesq l("Do you want to craft @@? For that I will need:", getitemlink(.@prize));
        mesc l("@@/@@ @@", countitem(.@base1), .@amon1, getitemlink(.@base1));
        mesc l("@@/@@ @@", countitem(.@base2), .@amon2, getitemlink(.@base2));
        mesc l("@@/@@ @@", countitem(.@base3), .@amon3, getitemlink(.@base3));
        mesc l("@@/@@ GP", format_number(Zeny), .@price);
        next;

        select
            l("Yes"),
            l("No");

        if (@menu == 2)
            return;

        if (countitem(.@base1) >= .@amon1 &&
            countitem(.@base2) >= .@amon2 &&
            countitem(.@base3) >= .@amon3 &&
            Zeny >= .@price) {
            inventoryplace .@prize, 1;
            delitem .@base1, .@amon1;
            delitem .@base2, .@amon2;
            delitem .@base3, .@amon3;
            Zeny = Zeny - .@price;
            getitem .@prize, 1;

            mes "";
            mesn;
            mesq l("Many thanks! Come back soon.");
        } else {
            speech S_FIRST_BLANK_LINE,// | S_LAST_NEXT,
                    l("You don't have enough material, sorry.");
        }
        return;        
    }

    mesn;
    if (is_night())
        mesq l("Good @@. My name is @@ and I make @@.", l("evening"), .name$, l("firearms"));
    else
        mesq l("Good @@. My name is @@ and I make @@.", l("day"), .name$, l("firearms"));
    next;
    if (BaseLevel < 60)
        goto L_TooWeak;
    else if (BaseLevel < 70)
        goto L_Weak;
    goto L_Menu;

L_TooWeak:
    mesn;
    mesq l("These weapons are only for masters, so you must get levels before being able to use them.");
    close;

L_Weak:
    mesn;
    mesq l("You need level 70 to use these guns, but if you want to start collecting materials, you're allowed to.");
    next;
    goto L_Menu;

L_Menu:
    menu
        l("I would like some information"), L_Info,
        l("I want a gun!"), L_Craft,
        l("I don't want anything right now, bye."), L_Close;

L_Info:
    mes "";
    mesn;
    mesq l("There are four basic class:");
    mesc l("REVOLVERS");
    mesc l("* The only one hand ranged weapon you'll ever find. Not amazing at all.");
    mesc l("RIFLES");
    mesc l("* Huge damage and more criticals, but slow fire rate.");
    mesc l("GATLING");
    mesc l("* Shoots like crazy, but deals less damage.");
    mesc l("SHOTGUNS");
    mesc l("* Causes splash damage, and are very expensive.");
    next;
    mesn;
    mesq l("Select carefully which weapon you want, so there are no regrets.");
    next;
    goto L_Menu;

L_Craft:
    select
        l("I changed my mind."),
        l("I want a REVOLVER."),
        l("I want a RIFLE."),
        l("I want a GATLING."),
        l("I want a SHOTGUN.");
    mes "";

    switch (@menu) {
        case 2:
            craft_gun( LeadIngot, 6, TitaniumIngot, 6, Coal, 12, PynRevolver, 15000 );
            goto L_Craft;
        case 3:
            craft_gun( LeadIngot, 8, TitaniumIngot, 8, Coal, 15, PynRifle, 15000 );
            goto L_Craft;
        case 4:
            craft_gun( LeadIngot, 8, TitaniumIngot, 8, Coal, 15, PynGatling, 15000 );
            goto L_Craft;
        case 5:
            craft_gun( LeadIngot, 12, TitaniumIngot, 12, Coal, 24, PynShotgun, 60000 );
            goto L_Craft;
    }
    goto L_Menu;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, WarlordHelmet);
    setunitdata(.@npcId, UDT_HEADMIDDLE, GoldenWarlordPlate);
    setunitdata(.@npcId, UDT_HEADBOTTOM, NPCEyes);
    setunitdata(.@npcId, UDT_WEAPON, BromenalPants);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 2);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 4);

    .sex=G_MALE;
    .distance=5;
    end;

}
