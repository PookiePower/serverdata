// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 008-1: 1st Floor - Party Dungeon mobs
008-1,155,138,15,15	monster	Ratto	1005,2,15000,15000
008-1,34,113,28,16	monster	Ratto	1005,2,15000,15000
008-1,52,147,42,15	monster	Angry Scorpion	1131,4,15000,25000
008-1,118,144,22,17	monster	Pinkie	1132,3,15000,25000
008-1,72,57,49,34	monster	House Maggot	1084,14,15000,25000
008-1,165,75,16,13	monster	Slime Blast	1090,2,15000,25000
008-1,37,54,29,26	monster	Red Scorpion	1072,2,15000,25000
008-1,132,108,42,15	monster	Fire Goblin	1067,4,15000,25000
008-1,157,158,17,15	monster	Mana Ghost	1068,1,15000,25000
008-1,154,46,29,15	monster	Mineral Bif	1058,2,15000,45000
008-1,147,143,29,27	monster	Squirrel	1032,4,15000,25000
008-1,146,44,37,30	monster	Maggot	1030,8,15000,25000
008-1,134,96,41,18	monster	Candor Scorpion	1073,6,15000,25000
008-1,66,41,42,15	monster	Bat	1039,5,15000,25000
008-1,60,78,35,22	monster	Duck	1029,4,15000,25000
008-1,52,133,37,31	monster	Cave Maggot	1027,10,15000,25000
008-1,103,144,12,15	monster	Little Blub	1007,3,15000,25000
008-1,99,54,79,33	monster	Piou	1002,8,15000,25000
008-1,100,108,79,21	monster	Piousse	1003,6,15000,25000
008-1,59,149,39,19	monster	Plushroom Field	1011,2,15000,25000
008-1,139,150,39,19	monster	Chagashroom Field	1128,2,15000,25000
