// TMW2 Script
// Author:
//    Saulc
// Description:
//  Laranja, orange in portuges is a orange dye seller in ship

016-1,25,24,0	script	Laranja	NPC_F_SHOPKEEPER,{

    mesn;
    mesq l("I'm Laranja.");
    next;
    mesn;
    mesq l("My father lead a dye fabric, I'm in charge to sell @@ around the world.", getitemlink(OrangeDye)); 
    next;
    mesn;
    mesq l("Would you like to buy some?");
    mes "";
    menu
        l("Yeah sure let's Trade."), -,
        l("Sorry another day maybe."), L_Close;

    // Restock shop if needed (ULTRA EXPERIMENTAL)
    // It won't work if instance_id() is 0, but that should be Nard@Candor.
    if (instance_id()) {
        if ('day != gettime(5)) {
            'day=gettime(5);
            restoreshopitem OrangeDye, -1, 3;
        }
    }

    closedialog;
    shop instance_npcname(.name$);
    close;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ShortTankTop);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 10);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 9);

    .sex = G_FEMALE;
    .distance = 4;
    tradertype(NST_MARKET);

    sellitem OrangeDye, -1, 3;
    end;

OnInstanceInit:
    .@npcId = getnpcid(0, instance_npcname(.name$));
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, ShortTankTop);
    setunitdata(.@npcId, UDT_HEADBOTTOM, RaidTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 10);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 9);
    tradertype(NST_MARKET);

    sellitem OrangeDye, -1, 3;
    'day=gettime(5);
    end;

OnClock2000:
    restoreshopitem OrangeDye, -1, 3;
    end;

}
