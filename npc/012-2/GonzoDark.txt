// TMW2 Script
// Author:
//  Jesusalva
// Description:
//      Monthly Sponsor Quest
// Variable:
//      SQuest_Sponsor
//      Quest ID: 2

012-2,50,81,0	script	GonzoDark Sponsor	NPC_LUCAS,{
    function resetSQS {
        setq SQuest_Sponsor, 0, gettime(GETTIME_MONTH);
    }
    // Check monthly quest
    .@d=getq2(SQuest_Sponsor);
    if (.@d != gettime(GETTIME_MONTH)) resetSQS();

    // Core
    .@q=getq(SQuest_Sponsor);
    mesn;
    mesq l("Ahh, I hate @@.", getmonsterlink(BlackScorpion));
    if (!(.@q & .questID)) goto L_Menu;
    close;

L_Menu:
    mesq l("I will reward you for 7 @@.", getitemlink(.itemID));
    mes "";
    menu
        rif(countitem(.itemID) >= 7, l("Here they are!")), L_Finish,
        l("Where can I find them?"),L_Where,
        l("No, thanks."),L_Close;

L_Finish:
    delitem .itemID, 7;
    Zeny=Zeny+700; // 100 GP each
    getitem StrangeCoin, rand(1,4);
    .@q=getq(SQuest_Sponsor);
    setq1 SQuest_Sponsor, .@q | .questID;
    mes "";
    mesn;
    mesq l("Many thanks! Come back later to bring me extra @@!", getitemlink(RedScorpionStinger));
    close;

L_Where:
    mes "";
    mesq l("Ah, there are lots on dark mines.");
    next;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    .questID=2;
    .itemID=BlackScorpionStinger;
    end;
}
