// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 007-1: Tulimshar Mining Camp mobs
007-1,140,101,43,90	monster	Cave Maggot	1027,38,35000,300000,Tycoon::OnKillCaveMaggot
007-1,129,45,14,9	monster	Black Scorpion	1074,4,35000,220000,Tycoon::OnKillBlackScorpion
007-1,70,60,19,15	monster	Ratto	1005,3,35000,300000,Tycoon::OnKillRatto
007-1,64,50,19,23	monster	Piou	1002,3,35000,300000
007-1,118,140,57,52	monster	Cave Snake	1035,25,35000,300000,Tycoon::OnKillCaveSnake
007-1,57,156,37,29	monster	Red Scorpion	1072,10,35000,300000,Tycoon::OnKillRedScorpion
007-1,39,125,7,6	monster	Cave Maggot	1027,5,35000,300000,Tycoon::OnKillCaveMaggot
007-1,89,72,75,57	monster	Cave Bat	1039,7,35000,300000
007-1,104,99,99,99	monster	Ruby Bif	1099,9,35000,300000
007-1,117,94,10,7	monster	Black Scorpion	1074,1,35000,300000,Tycoon::OnKillBlackScorpion
007-1,93,110,9,9	monster	Cave Maggot	1027,8,35000,300000,Tycoon::OnKillCaveMaggot
