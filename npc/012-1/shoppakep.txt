// TMW2 Script.
// Author:
//    Jesusalva
// Description:
//    Random Hurnscald Shopper with random overpriced stuff and gifts

012-1,58,64,0	script	Shoppa Kep	NPC_SHOPPAKEP,{
    mesn;
    mesq lg("Welcome! Only the finest wares!");
    next;
    closedialog;
    shop .name$;
    close;

    function ShoppaKepItem {
    .@i=rand(0,100);
    if (.@i > 75)
        return Aquada;
    else if (.@i < 35)
        return Manana;
    else
        return HalfEggshell;
    }

OnInit:
    .CurrentItem=ShoppaKepItem();
    .ThisRebootAmou=rand(30,70);

    .sex = G_FEMALE;
    .distance = 5;


    tradertype(NST_MARKET);
    sellitem CottonSkirt, 100000, 1;
    sellitem MiniSkirt, 100000, 1;
    sellitem ShortTankTop, 100000, 1;
    sellitem CottonTrousers, 100000, 1;

    sellitem Piberries, -1, 30;
    sellitem Plushroom, -1, 3;
    sellitem Chagashroom, -1, 3;

    sellitem PiouFeathers, -1, .ThisRebootAmou;
    sellitem .CurrentItem, -1, rand(1,4);

    end;

OnClock1759:
OnClock0546:
    restoreshopitem LeatherShirt, 125000, 1;
    restoreshopitem JeansShorts, 125000, 1;
    restoreshopitem CottonSkirt, 100000, 1;
    restoreshopitem MiniSkirt, 100000, 1;
    restoreshopitem ShortTankTop, 100000, 1;
    restoreshopitem CottonTrousers, 100000, 1;

    restoreshopitem Piberries, -1, 30;
    restoreshopitem Plushroom, -1, 3;
    restoreshopitem Chagashroom, -1, 3;

    restoreshopitem PiouFeathers, -1, .ThisRebootAmou;

    stopselling(.CurrentItem);
    .CurrentItem=ShoppaKepItem();
    sellitem .CurrentItem, -1, rand(1,4);
    end;
}
