// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Soul Menhir

012-1,88,69,0	script	Soul Menhir#hurns	NPC_SOUL_NORMAL,{
    @map$ = "012-1";
    setarray @Xs, 86, 87, 86, 87;
    setarray @Ys, 69, 70, 69, 70;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
