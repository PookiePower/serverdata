// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    These statues are of great honor to whoever have their name written in them!

012-1,84,63,0	script	Hero Statue#012-1	NPC_STATUE_ANDREI,{

    mes l("This statue was built for memory of Andrei Sakar, the greatest hero this world has even seen.");
    mes l("For defending Hurnscald alone and saving all its inhabitants.");
    mes l("For fighting against the Monster King once and getting out alive to tell the story.");
    mes l("For all his great deeds, and thousands of lives he saved, this statue is in his honor.");
    if ($MOST_HEROIC$ == "")
        goto L_Fame;
    next;
    mes l("Also in honor of @@, who did a great act of bravery recently. May they keep protecting our world!", $MOST_HEROIC$);

L_Fame:
    next;
    mesq l("All hail the ones who proven their worth before the whole Alliance!");

    HallOfFame();
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;
}

