// TMW-2 Script.
// Author:
//    Saulc
//    Jesusalva
// Notes:
//    Buys Tonori Delight

009-2,29,57,0	script	Ryan	NPC_PLAYER,{
    if (BaseLevel < 17) goto L_Weak;
    .@q=getq(HalinarzoQuest_LifeDelight);
    .@a=(.@q**2)+20;
    // Maximum: 10'020 Delights
    if (.@q > 100) goto L_Complete;
    mesn;
    mesq l("Ah, I just wanted @@... Yummy, lovely @@...", getitemlink(TonoriDelight), getitemlink(TonoriDelight));
    next;
    mesn;
    mesq l("If you bring me @@ @@, I'll give you a reward!", .@a, getitemlink(TonoriDelight));
    next;
    select
        rif(countitem(TonoriDelight) >= .@a, l("I have everything!")),
        l("Ah, not now...");
    mes "";
    if (@menu == 2)
        close;

    inventoryplace ChefHat, 1;

    delitem TonoriDelight, .@a;
    getexp rand(.@a, .@a*2)+.@q*rand(10,30), 0;
    Zeny=Zeny+.@a*21;
    setq HalinarzoQuest_LifeDelight, .@q+1;
    mesn;
    mesq l("Hmmm... Yummy!");

    // Landmark rewards
    // Chef hat last stage: 120 Delights - 605 in total
    .@q=getq(HalinarzoQuest_LifeDelight);
    if (.@q == 11) {
        getitem ChefHat, 1;
        getexp 13500, 10;
        next;
        mesn;
        mesq l("Ah, you really deserve this @@. Good job!", getitemlink(ChefHat));
    }        
    if (.@q >= 21) {
        Zeny=Zeny+rand(1,.@a);
        getexp rand(1,.@q), rand(1, .@q);
    }        
    if (.@q >= 51) {
        Zeny=Zeny+rand(.@q,.@a);
        getexp rand(1,.@a), rand(1, .@q);
    }        
    if (.@q >= 91) {
        Zeny=Zeny+rand(.@a, .@a+.@q);
        getexp rand(.@q,.@a), rand(1, .@q);
    }        
    if (.@q == 100) {
        getitem PlatinumIngot, 1;
        next;
        mesn;
        mesq l("Ah, I have enough @@ for a life! Take this EXTREMELY RARE @@. Good job!", getitemlink(TonoriDelight), getitemlink(PlatinumIngot));
    }        

    close;

L_Complete:
    mesn;
    mesq l("Thanks for the help! Yummy!");
    close;

L_Weak:
    mesn;
    mesq l("How did you even got here? There are dangerous swamps north, and dangerous deserts south...");
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, ChefHat);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    setunitdata(.@npcId, UDT_WEAPON, Boots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 26);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 2);

    .sex = G_MALE;
    .distance = 4;
    end;
}

