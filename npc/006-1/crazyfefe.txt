// TMW2 Script
// Author:
//    TMW Org. (original code)
//    Jesusalva
// Description:
//    Crazyfefe will wake up when he smells battle
// Variables:
//      $@FEFE_CAVE_LEVEL
//          Tracks if a fight is going on, and the current round if true.
//      $@FEFE_DELAY
//          Tracks the delay between challenges. Also used to start the combat. Set it to zero so you can start fight at once.
//      $@FEFE_CAVE_HERO$
//          Whoever started a fight. Also used to know if a fight is about to begin.
//      $@FEFE_CAVE_PLAYER_COUNT
//          How many players are there
//      $@FEFE_WAVE
//          Current wave
//      $@GM_OVERRIDE
//          Only possible with @set command, overrides the co-op requeriment. Affects other scripts.

006-1,47,22,0	script	Crazyfefe	NPC_STATUE_EVILMAN,{
    if ($@FEFE_CAVE_LEVEL) goto L_Enjoy;
    if ($@FEFE_DELAY > gettimetick(2)) goto L_Wait;
    mesn;
    mesq l("Who dares to disturb my slumber?");
    next;
    menu
        l("Sorry, sorry! Please rest, great Crazyfefe!"), L_Exit,
         l("I do. I want to challenge the Candor Cave!"), L_Next,
         l("What is this place anyway?"), L_Explain;

L_Next:
    mesn;
    mesq l("Very well, but for a fee of @@ GP. There's no free lunch, after all!", .price);
    select
         l("Sorry, I misclicked the first button. Have a nice nap, great Crazyfefe!"),
         l("Yeah, I have the money. Give me FIVE minutes, and Bring it on!"),
         l("Yeah, I have the money. Give me TEN minutes, and Bring it on!"),
         l("Yeah, I have the money. Give me FIFTEEN minutes, and Bring it on!"),
         "","",
         rif(is_gm(), l("I'm GM and got the money. Gimme HALF HOUR to invite everybody!"));

    // Cancel?
    if (@menu == 1)
        goto L_Exit;

    // Already started?
    if ($@FEFE_CAVE_LEVEL || $@FEFE_CAVE_HERO$ != "")
        goto L_AlreadyStarted;

    // Enough money?
    if (Zeny < .price)
        goto L_NotEnough;

    Zeny = Zeny - .price;
    // Time delay
    $@FEFE_DELAY = gettimetick(2)+300*(@menu-1);
    $@FEFE_CAVE_HERO$ = strcharinfo(0);
    initnpctimer;
    announce $@FEFE_CAVE_HERO$+" invites everyone to a challenge against Candor Cave. It'll start in "+FuzzyTime($@FEFE_DELAY,2,2)+". Prepare yourselves!", bc_all|bc_npc;
    mesn;
    mesq l("Please wait, the fight will start in @@, as requested.", FuzzyTime($@FEFE_DELAY,2,2));
    close;

L_StartFight:
    if (getmapusers("006-1") < 3 && !$@GM_OVERRIDE)
        goto L_NotEnoughPlayers;

    $@FEFE_CAVE_LEVEL = 1;
    $@FEFE_DELAY = gettimetick(2)+.delay;
    $@FEFE_CAVE_PLAYER_COUNT = getmapusers("006-1");
    mapannounce("006-1", "The battle is about to begin!", bc_map);
    killmonsterall("006-1"); // Saulc likes to spawn stuff to heat stuff up
    initnpctimer;
    end;

L_Enjoy:
    npctalk3 l("Pay attention to the fight, @@!", strcharinfo(0));
    if (is_gm())
        dispbottom l("Round @@", $@FEFE_CAVE_LEVEL);
    end;

L_NotEnough:
    mesn;
    mesq l("You lack money. Go sell your stuff. And don't ask why a statue needs money!");
    close;

L_AlreadyStarted:
    mesn;
    mesq l("Sorry, @@ already started the fight.", $@FEFE_CAVE_HERO$);
    close;

L_Wait:
    .@time$=FuzzyTime($@FEFE_DELAY,2,2);
    if ($@FEFE_DELAY-gettimetick(2) <= 30) {
        npctalk3 l("@@!", .@time$);
        end;
    }
    mesn;
    mesq l("Be Patient... You still need to wait @@.", .@time$);
    goto L_Exit;

L_NotEnoughPlayers:
    mapannounce("006-1", "Oh noes! There's not enough players. Fight aborted, no refunds!", bc_map);
    goto L_CleanUp;

L_Explain:
    mesn;
    mesq l("I am @@, guardian of Candor Cave. Below this cave, lies the legendary Fefe, arch-wizard from the Great War.", .name$);
    next;
    mesn;
    mesq l("During the war, the Monster King cursed the place where he died, but he fought back, and cursed the curse.");
    next;
    mesn;
    mesq l("For short, they had a fight with curses, and now there's a talking statue over his grave which can spawn monsters here.");
    next;
    mesn;
    mesq l("If you pay the fee, I'll spawn them, for no longer than 20 minutes. You should not fight alone, instead, you need a group of at least three, because, you know, the curse. Or something.");
    next;
    menu
        l("I need to think about this."), L_Exit,
        l("I'm interested."), L_Next;

L_Exit:
    close;







// Check if we're ready for next wave. Otherwise, do this check again after 5 seconds.
L_CaveLogic:
    .wtime+=5; // This is looped every 5 s
    $@FEFE_CAVE_PLAYER_COUNT = getmapusers("006-1");

    // New wave condition: All mobs dead, or 5 minutes have passed
    if (mobcount("006-1", "Crazyfefe::OnPetDeath") <= 0 || .wtime >= 240)
        goto L_NextRound;

    // Victory conditions: Too few players are alive OR twenty minutes went on.
    // WARNING, dead players still in cave are counted, we might never finish the fight.
    // And a GM might be called to ban everyone inside.
    if ($@FEFE_CAVE_PLAYER_COUNT <= any(1,1,1,1,1,1,1,1,1,2) && !$@GM_OVERRIDE || gettimetick(2) >= $@FEFE_DELAY - (60*20)) {
        goto L_CleanUp;
    }

    // reset timer
    initnpctimer;
    end;

L_NextRound:
    .wtime=0;
    $@FEFE_WAVE = $@FEFE_WAVE + 1;
    // Prepare next round, and reward survivors
    $@FEFE_CAVE_LEVEL = $@FEFE_CAVE_LEVEL + $@FEFE_CAVE_PLAYER_COUNT + rand(1,3) + $@FEFE_WAVE;
    areatimer "006-1", 20, 20, 70, 60, 10, "Crazyfefe::OnReward";

    mapannounce "006-1", "The wave nº "+$@FEFE_WAVE+" is starting with " + $@FEFE_CAVE_PLAYER_COUNT + " player(s) left alive." , 0;
    goto L_Summon;

// TODO
L_Summon:
    .@amount=($@FEFE_CAVE_LEVEL/rand(4,6))+1;
    freeloop(true);
	for (.@i = 0; .@i < .@amount; ++.@i) {
        .@mid=rand(1,22);
        .@monsterId=Piou;
        switch (.@mid) {
            case 1:
                .@monsterId = CaveMaggot ; break;
            case 2:
                //.@monsterId = Wolvern ; break;
                .@monsterId = FallenGuard2 ; break;
            case 3:
                .@monsterId = DarkLizard ; break;
            case 4:
                .@monsterId = MagicGoblin ; break;
            case 5:
                .@monsterId = Bandit ; break;
            case 6:
                .@monsterId = GreenSlime ; break;
            case 7:
                .@monsterId = LavaSlime ; break;
            case 8:
                .@monsterId = Snake ; break;
            case 9:
                .@monsterId = DesertBandit ; break;
            case 10:
                .@monsterId = Sarracenus ; break;
            case 11:
                .@monsterId = AngryRedScorpion ; break;
            case 12:
                .@monsterId = BlackScorpion ; break;
            case 13:
                .@monsterId = FallenGuard1 ; break;
            case 14:
                .@monsterId = Yeti ; break;
            case 15:
                .@monsterId = BlackSlime ; break;
                //.@monsterId = JackO ; break;
            case 16:
                .@monsterId = RobinBandit ; break;
            case 17:
                .@monsterId = CandiedSlime ; break;
            case 18:
                .@monsterId = BlackMamba ; break;
            case 19:
                .@monsterId = WickedMushroom ; break;
            case 20:
                .@monsterId = BlackSlime ; break;
            case 21:
                .@monsterId = RedMushroom ; break;
            default:
                .@monsterId = AngryScorpion ; break;
        }
        areamonster "006-1", 20, 20, 70, 60, strmobinfo(1, .@monsterId), .@monsterId, 1, "Crazyfefe::OnPetDeath";
    }
    freeloop(false);
    initnpctimer;
    end;

L_CleanUp:
    mapannounce "006-1", "Game over! Who will be the next to fall on Crazyfefe's Cave?", 0;

    areatimer "006-1", 20, 20, 70, 60, 10, "Crazyfefe::OnReward";
    $@FEFE_CAVE_LEVEL = 0;
    $@FEFE_WAVE = 0;
    $@FEFE_CAVE_HERO$ = "";
    .wtime=0;
    killmonster "006-1", "Crazyfefe::OnPetDeath";
    stopnpctimer;
    initnpctimer;
    stopnpctimer;
    end;








// Rewards surviving players between rounds, according to performance, and get rid of dead PCs.
OnReward:
    if (ispcdead()) {
        recovery();
        warp "Save", 0, 0;
        end;
    }
    // Player reward is a random number based on current wave + lvl/50 (1 point ~= 12~8 mobs)
    .@prize=rand($@FEFE_WAVE/2,$@FEFE_WAVE) + ($@FEFE_CAVE_LEVEL/50);
    Zeny=Zeny+.@prize;
    getexp .@prize, rand(1,3);
    end;

// Every 5 seconds, handle cave, if fighting. Does nothing when waiting.
OnTimer5000:
    if ($@FEFE_CAVE_LEVEL)
        goto L_CaveLogic;
    end;

// Announces and attempts to start the fight once time run out
//OnTimer30000: // If you need to start without 5 minutes delay, this line must be uncommented.
OnTimer300000:
    if ($@FEFE_CAVE_LEVEL > 0) end;
    if ($@FEFE_DELAY <= gettimetick(2)) goto L_StartFight;
    announce $@FEFE_CAVE_HERO$+" invites everyone to a challenge against Candor Cave. It'll start in "+FuzzyTime($@FEFE_DELAY,2,2)+". Prepare yourselves!", bc_all|bc_npc;
    initnpctimer;
    end;

OnPetDeath:
    end;

OnInit:
    .sex=G_OTHER;
    .distance=5;
    .price=6000;
    .delay=(60*40);
    .wtime=0;
    end;
}
