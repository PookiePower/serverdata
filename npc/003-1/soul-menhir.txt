// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Soul Menhir

003-1,40,48,0	script	Soul Menhir#tulim	NPC_SOUL_DESERT,{
    @map$ = "003-1";
    setarray @Xs, 40, 41, 40;
    setarray @Ys, 49, 49, 49;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
