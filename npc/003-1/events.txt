// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    The NPCs on this file are for when events end. Must be manually enabled.

// Easte
003-1,47,53,0	script	Lilica#final	NPC_EASTER,{
    mesn;
    mesq l("Easter is over! I am the last chance to get rid of eggs!!");

    menu
        l("Trade Silver Eggs"), L_Silver,
        l("Trade Golden Eggs"), L_Golden,
        l("View LeaderBoard"), L_Leader,
        l("Thanks Lilica."), -;
    close;

L_Silver:
    openshop "#eastershop1";
    closedialog;
    close;

L_Golden:
    openshop "#eastershop2";
    closedialog;
    close;

L_Leader:
	.@nb = query_sql("SELECT c.name, i.amount FROM `inventory` AS i, `char` AS c WHERE i.nameid=834 AND i.char_id=c.char_id ORDER BY i.amount DESC LIMIT 10", .@name$, .@value);

    mesn l("Players who still have Golden Eggs");
	mes("1."+.@name$[0]+" ("+.@value[0]+")");
	mes("2."+.@name$[1]+" ("+.@value[1]+")");
	mes("3."+.@name$[2]+" ("+.@value[2]+")");
	mes("4."+.@name$[3]+" ("+.@value[3]+")");
	mes("5."+.@name$[4]+" ("+.@value[4]+")");
	mes("6."+.@name$[5]+" ("+.@value[5]+")");
	mes("7."+.@name$[6]+" ("+.@value[6]+")");
	mes("8."+.@name$[7]+" ("+.@value[7]+")");
	mes("9."+.@name$[8]+" ("+.@value[8]+")");
	mes("10."+.@name$[9]+" ("+.@value[9]+")");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 5;

    disablenpc(.name$);
    end;
}

// Whatever event use #RARE_POINTS
// Which is a script variable, meaning it will be a really special event.
// Side Note: Might as well add a special cap for such situations.
003-1,47,53,0	script	Pydisgner#spoints	NPC_GUGLI,{
    openshop;

OnInit:
    .sex = G_OTHER;
    .distance=5;

	tradertype(NST_CUSTOM);

    // Rares
	sellitem BlacksmithAxe, 64000;
	sellitem MiereCleaver,  32000;
	sellitem Kanabo,        13500;
	sellitem PiouEgg,        9350;
	sellitem AxeHat,         8000;
	sellitem Googles,        6000;

    // Uncommon
	sellitem CrystallizedMaggot, 7000;
	sellitem Sunglasses,         7000;
	sellitem BunnyEars,           900;
	sellitem StrangeCoin,          10;

    // Gamble
	sellitem SupremeGift,30000;
	sellitem PrismGift,  11000;
	sellitem GoldenGift,  4000;
	sellitem SilverGift,  1500;
	sellitem BronzeGift,   500;
	sellitem CasinoCoins,   35;

    // Equipment
	sellitem WoodenSword,      5500;
    sellitem Chainmail,        2500;
    sellitem LeatherShirt,      800;
    sellitem JeansShorts,       600;
    sellitem ShortBow,          500;
    sellitem RoundLeatherShield,200;
    sellitem WoodenBow,         100;
    sellitem Dagger,            100;

    // Consumables
	sellitem TonoriDelight, 80;
	sellitem Aquada,        60;
	sellitem Manana,        50;
	sellitem Cheese,        40;
	sellitem Plushroom,     15;
	sellitem Chagashroom,   15;

    // Useful ores and money stuff
	sellitem IronIngot, 2500;
    sellitem EmptyBottle, 60;
    sellitem Arrow,        4;
    sellitem TolchiArrow,  1;
	sellitem PiouFeathers, 1;


    if (gettime(7) != 2018)
        disablenpc(.name$);
    end;

OnCountFunds:
	setcurrency(#RARE_POINTS);
	end;

OnPayFunds:
	if( #RARE_POINTS < @price )
		end;
    #RARE_POINTS=#RARE_POINTS-@price;
	purchaseok();
	end;

}
