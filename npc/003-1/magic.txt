// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Controlled Warp to Magic School (+whatever magic stuff we need to add later)

003-1,34,33,0	script	#MagicSchoolGate1	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    if (countitem(JesusalvaGrimorium) <= 0 || MAGIC_LVL < 2) {
        slide 36, 39;
        percentheal -5, -10;
        dispbottom l("Powerful magic repels you away from this magic place!");
    } else {
        warp "003-0", 48, 51;
    }
    end;
}
