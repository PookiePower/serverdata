// TMW-2 Script
// Author:
//    Jesusalva
// Description:
//    This stuff should be moved somewhere else...

003-1,89,109,0	script	Michel	NPC_BACCHUS,{

    mesn;
    mesq l("These people have lots of unspent Strange Coins! Waw!");

    HallOfCoins();

    // Anyone with GM Level, staff or not, is allowed to latest GM logs
    if (!getgmlevel()) close;

    next;
    HallOfGMLog();

    // Only Admins are allowed to change the HoF Scoreboard
    if (!is_admin()) close;

    next;
    mes "##BGM Information##b";
    mes "We are currently relying in Karma to generate this Hall Of Fame.";
    mes "";
    mes "##B Manual Edition ##b";
    mes "To reliably change Karma values, you must use @set Karma.";
    mes "But watch out: This command overrides karma!";
    mes "Therefore, it is advised to DON'T change this by hand.";
    mes "";
    mes "##B Scripting Edition ##b";
    mes "Is known to work. This hall of fame will need a few minutes to update itself.";
    mes "";
    mes "##B Technical Notes ##b";
    mes "In future we may start Fame field directly (but it is not displayed on client, thus, we decided to use Karma).";
    //set Karma, Karma + 5;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
