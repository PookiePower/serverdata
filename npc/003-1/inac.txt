// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva

003-1,55,82,0	script	Inac	NPC_PLAYER,{
    mesn;
    mesq l("Many stories are told about this city sewers.");
    next;
    mesn;
    mesq l("One of them is really crazy, though: They say that there is a legendary monster down there.");
    next;
    mesn;
    mesq l("That's just nonsense though, I've been there thousands of times and saw no such thing.");
    if (getq(TulimsharQuest_Sewers) > 0)
        close;
    next;
    mesn;
    mesq l("Anyway, there's a barrier to prevent monsters from attacking the city from below.");
    next;
    mesn;
    mesq l("To enter on them, you need to be level 25 and use the following chant: \"Blame Saulc\". I don't know why, but that open doors!");
    setq TulimsharQuest_Sewers, 1;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, Bandana);
    setunitdata(.@npcId, UDT_HEADMIDDLE, VneckJumper);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 7);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_MALE;
    .distance = 5;
    end;
}
