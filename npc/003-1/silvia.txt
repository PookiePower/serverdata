// TMW2 Script.
// Author:
//    Saulc
//    Jesusalva

003-1,109,150,0	script	Silvia	NPC_FEMALE,{

    if (strcharinfo(0) == $MOST_HEROIC$) npctalk l("Oh my, the great @@ has come to talk to me!", $MOST_HEROIC$);
    if (getq(TulimsharQuest_Swezanne) == 4) goto L_Message;
    .@q2=getq2(TulimsharQuest_Swezanne);
    if (.@q2 < santime()) goto L_Unallowed;
    if (getq(TulimsharQuest_Swezanne) == 1 && getq(TulimsharQuest_Lifestone) < 2) goto L_Lifestone;
    if (strcharinfo(0) != $MOST_HEROIC$) hello;
    end;

L_Message:
    mesn strcharinfo(0);
    mesq l("Your mother asked me to say that she loves you.");
    next;
    mesn;
    mesq l("Oh no, not another stranger she sends me to tell that!");
    next;
    mesn;
    mesq l("She never leaves the shade of that tree, she is always sending messages by other people!!");
    next;
    mesn;
    mesq l("Oh well... That's my mother, and this is why I love her.");
    next;
    inventoryplace CottonGloves, 1;
    getitem CottonGloves, 1;
    getexp 105,0;
    setq TulimsharQuest_Swezanne, 1;
    mesn;
    mesq l("Thank you, @@. Please take this pair of gloves as a thank you.", strcharinfo(0));
    close;

L_Lifestone:
    mesn;
    mesq l("Hey! Good to see you. I was thinking how I could repay for what you've done for my mother.");
    next;
    mesn;
    mesq l("I can make you a @@, and for that I will want a @@ and 500 GP.", getitemlink(LifestonePendant), getitemlink(Lifestone));
    if (getq(TulimsharQuest_Lifestone) == 0) {
        next;
        mesn;
        mesq l("I am not sure of who makes or haves Lifestones. Try looking outside the city. Who knows.");
        close;
    }
    menu
        rif(Zeny >= 500 && countitem(Lifestone) > 0, l("Yes, I accept the pendant!")), -,
        l("Not now, but I may be back later."), L_Close;

    // Whaaaat, this is a major error affecting several scripts!
    if (Zeny < 500 || countitem(Lifestone) < 0) {
        atcommand("@request Someone is cheating, call Jesusalva at once!");
        atcommand("@ban \""+strcharinfo(0)+"\" 15mn"); // I truly hope they bother Jesusalva they were banned
        disablenpc "Silvia";
        close;
    }
    Zeny-=500;
    delitem Lifestone, 1;
    getitem LifestonePendant, 1;
    setq(TulimsharQuest_Lifestone, 2);
    mes "";
    mesn;
    mesq l("There you go! Thanks for all the help!");
    close;

L_Unallowed:
    mesn;
    mesq l("Ah, I wonder how my mother Swezanne is faring...");
    next;
    mesn;
    mesq l("She must be thristy, fighting monsters on this sun... If somebody could give her Cactus Potions...");
    close;

L_Close:
    closedialog;
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, LeatherTrousers); // TODO
    setunitdata(.@npcId, UDT_WEAPON, LousyMoccasins);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 21);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 11);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
