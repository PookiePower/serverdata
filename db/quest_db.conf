quest_db: (
//  Quest Database
/******************************************************************************
 ************* Entry structure ************************************************
 ******************************************************************************
{
	Id: Quest ID                    [int]
	Name: Quest Name                [string]
	TimeLimit: Time Limit (seconds) [int, optional]
	Targets: (                      [array, optional]
	{
		MobId: Mob ID           [int]
		Count:                  [int]
	},
	... (can repeated up to MAX_QUEST_OBJECTIVES times)
	)
	Drops: (
	{
		ItemId: Item ID to drop [int]
		Rate: Drop rate         [int]
		MobId: Mob ID to match  [int, optional]
	},
	... (can be repeated)
	)
},
******************************************************************************/

// ID 0 to 10: Prologue quests
{
	Id: 0
	Name: "ShipQuests_Julia"
},
{
	Id: 1
	Name: "ShipQuests_Arpan"
},
{
	Id: 2
	Name: "ShipQuests_Alige"
},
{
	Id: 3
	Name: "ShipQuests_Peter"
},
// ID 4 is free
{
	Id: 5
	Name: "ShipQuests_Knife"
},
{
	Id: 6
	Name: "ShipQuests_ArpanMoney"
},
{
	Id: 7
	Name: "ShipQuests_ChefGado"
},
{
	Id: 8
	Name: "ShipQuests_Dan"
},
{
	Id: 9
	Name: "ShipQuests_Bottle"
},

// ID 11 to 30: General quests
// ID 11 is free
{
	Id: 12
	Name: "General_Narrator"
},
{
	Id: 13
	Name: "General_Banker"
},
{
	Id: 14
	Name: "General_Guild"
},
{
	Id: 15
	Name: "General_Hunter"
},
{
	Id: 16
	Name: "General_Auldsbel"
},

// ID 31 to 50: Candor Quests
{
    Id: 31
    Name: "CandorQuest_Chest"
},
{
    Id: 32
    Name: "CandorQuest_HAS"
},
{
    Id: 33
    Name: "CandorQuest_Tolchi"
},
{
    Id: 34
    Name: "CandorQuest_Maya"
},
{
    Id: 35
    Name: "CandorQuest_Rosen"
},
{
    Id: 36
    Name: "CandorQuest_Barrel"
},
{
    Id: 37
    Name: "CandorQuest_Sailors"
},
{
    Id: 38
    Name: "CandorQuest_Vincent"
},
{
    Id: 39
    Name: "CandorQuest_Trainer"
},
{
    Id: 40
    Name: "CandorQuest_Nurse"
},
{
    Id: 41
    Name: "CandorQuest_Liana"
},
{
    Id: 42
    Name: "CandorQuest_SailorCure"
},

// ID 51 to 70: Tulimshar quests
{
	Id: 51
	Name: "TulimsharQuests_Fishman"
},
{
    Id: 52
    Name: "TulimsharQuest_Sarah"
},
{
    Id: 53
    Name: "TulimsharQuest_WaterForGuard"
},
{
    Id: 54
    Name: "TulimsharQuest_Swezanne"
},
{
    Id: 55
    Name: "TulimsharQuest_Lifestone"
},
{
    Id: 56
    Name: "TulimsharQuest_Eistein"
},
{
    Id: 57
    Name: "TulimsharQuest_Hasan"
},
{
    Id: 58
    Name: "TulimsharQuest_Devoir"
},
{
    Id: 59
    Name: "TulimsharQuest_Sailors"
},
{
    Id: 60
    Name: "TulimsharQuest_DarkInvocator"
},
{
    Id: 61
    Name: "TulimsharQuest_AnwarField"
},
{
    Id: 62
    Name: "TulimsharQuest_Sewers"
},
{
    Id: 63
    Name: "TulimsharQuest_Neko"
},
{
    Id: 64
    Name: "TulimsharQuest_Alvasus"
},

// ID 71 to 90: Halinarzo Quests//
{
    Id: 71
    Name: "HalinarzoQuest_Foxhound"
},
{
    Id: 72
    Name: "HalinarzoQuest_TraderKing"
},
{
    Id: 73
    Name: "HalinarzoQuest_SickWife"
},
{
    Id: 74
    Name: "HalinarzoQuest_LifeDelight"
},
{
    Id: 75
    Name: "HalinarzoQuest_Sawis"
},

// ID 91 to 110: Hurnscald Quests
{
    Id: 91
    Name: "HurnscaldQuest_ForestBow"
},
{
    Id: 92
    Name: "HurnscaldQuest_HarkEye"
},
{
    Id: 93
    Name: "HurnscaldQuest_Celestia"
},
{
    Id: 94
    Name: "HurnscaldQuest_TeaParty"
},
{
    Id: 95
    Name: "HurnscaldQuest_Farmers"
},
{
    Id: 96
    Name: "HurnscaldQuest_Arkim"
},
{
    Id: 97
    Name: "HurnscaldQuest_Bandits"
},

// ID 111 to 130: Nivalis Quests
// ID 131 to 150: Frostia Quests
{
    Id: 131
    Name: "FrostiaQuest_WolfNecklace"
},
// ID 151 to 170: Artis Quests
// ID 171 to 190: Esperia Quests
// ID 191 to 269: Dungeon Quests
{
    Id: 191
    Name: "MineQuest_Tycoon"
},
{
    Id: 192
    Name: "MineQuest_Dracoula"
},
{
    Id: 193
    Name: "MineQuest_Caelum"
},

// ID 270 to 299: Land Of Fire Quests
{
    Id: 270
    Name: "LoFQuest_EPISODE"
},
{
    Id: 271
    Name: "LoFQuest_George"
},
{
    Id: 272
    Name: "LoFQuest_Fairy"
},
{
    Id: 273
    Name: "LoFQuest_Doug"
},

// ID 300 to 320: Seasonal/Annual/Monthly quests
{
    Id: 300
    Name: "SQuest_Summer"
},
{
    Id: 301
    Name: "SQuest_Autumn"
},
{
    Id: 302
    Name: "SQuest_Winter"
},
{
    Id: 303
    Name: "SQuest_Spring"
},
{
    Id: 304
    Name: "SQuest_Sponsor"
},
{
    Id: 305
    Name: "SQuest_Ched"
},
{
    Id: 306
    Name: "SQuest_Paxel"
},

// ID 1000+: Test quests
{
	Id: 1000
	Name: "Test_testing1"
},
)
